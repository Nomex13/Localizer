﻿using System;

namespace Iodynis.Libraries.Localizing
{
    public class LocalizerFunctionEnumeration : LocalizerFunction
    {
        /// <summary>
        /// Takes value as an index for the provided arguments, starting at 0, and returns the argument by that very index.
        /// </summary>
        /// <param name="name">Name of the function.</param>
        public LocalizerFunctionEnumeration(string name = "Enum")
            : base(name)
        {
            ;
        }
        public override object Invoke(string language, params object[] arguments)
        {
            if (arguments.Length < 2)
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated. It does not support {arguments.Length} arguments.");
            }

            int index = 0;
            try
            {
                index = Convert.ToInt32(arguments[0]);
            }
            catch (Exception exception)
            {
                throw new Exception($"Failed to cast {arguments[0]} to an integer index.", exception);
            }
            // Actual index begins from 1 as the 0 argument is the index itself
            index++;
            if (index <= 0)
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated for value {arguments[0]}. Index {arguments[0]} is negative.");
            }
            if (arguments.Length <= index)
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated for value {arguments[0]}. Index {arguments[0]} higher than number of possible variants.");
            }
            if (arguments[index] == null)
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) cannot be evaluated for a null value.");
            }
            return arguments[index];
        }
        //public override string Invoke(string language, params string[] arguments)
        //{
        //    if (arguments.Length < 2)
        //    {
        //        throw new Exception($"Function Enum({String.Join(", ", arguments)}) cannot be evaluated. It does not support {arguments.Length} arguments.");
        //    }

        //    int index;
        //    if (!Int32.TryParse(arguments[0], out index))
        //    {
        //        throw new Exception($"Function Enum({String.Join(", ", arguments)}) cannot be evaluated for value {arguments[0]}. Index {arguments[0]} is not a number.");
        //    }
        //    if (index < 0)
        //    {
        //        throw new Exception($"Function Enum({String.Join(", ", arguments)}) cannot be evaluated for value {arguments[0]}. Index {arguments[0]} is negative.");
        //    }
        //    if (arguments.Length <= index)
        //    {
        //        throw new Exception($"Function Enum({String.Join(", ", arguments)}) cannot be evaluated for value {arguments[0]}. Index {arguments[0]} higher than number of possible variants.");
        //    }
        //    if (arguments[index] == null)
        //    {
        //        throw new Exception($"Function Enum({String.Join(", ", arguments)}) cannot be evaluated for a null value.");
        //    }
        //    return arguments[index];
        //}
    }
}
