﻿using System;

namespace Iodynis.Libraries.Localizing
{
    public class LocalizerFunctionLanguage : LocalizerFunction
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="name">Name of the function.</param>
        public LocalizerFunctionLanguage(string name = "Language")
            : base(name)
        {
            ;
        }
        public override object Invoke(string language, params object[] arguments)
        {
            if (arguments.Length < 2)
            {
                throw new Exception($"Function {Name}({String.Join(", ", arguments)}) requires at least two argument.");
            }

            if (arguments.Length == 2)
            {
                return Localizer.LocalizeKey(arguments[0].ToString(), arguments[1].ToString());
            }
            else
            {
                string[] values = new string[arguments.Length - 2];
                for (int i = 2; i < arguments.Length; i++)
                {
                    values[i - 2] = arguments[i].ToString();
                }
                return Localizer.LocalizeKey(arguments[0].ToString(), arguments[1].ToString(), values);
            }
        }
        //public override string Invoke(string language, params string[] arguments)
        //{
        //    if (arguments.Length < 2)
        //    {
        //        throw new Exception($"Function Language({String.Join(", ", arguments)}) requires at least two argument.");
        //    }

        //    if (arguments.Length == 2)
        //    {
        //        return Localizer.LocalizeKey(arguments[0], arguments[1]);
        //    }
        //    else
        //    {
        //        return Localizer.LocalizeKey(arguments[0], arguments[1], Slice(arguments, 2));
        //    }

        //    //// Use default laguage
        //    //if (arguments.Length == 0)
        //    //{
        //    //    return Localizer.Csv[language, value];
        //    //}
        //    //else if (arguments[0] == null)
        //    //{
        //    //    throw new Exception($"Function Language({String.Join(", ", arguments)}) cannot be evaluated for a null value.");
        //    //}
        //    //// Use explicitly specified language
        //    //else if (arguments.Length == 1)
        //    //{
        //    //    return Localizer.Csv[arguments[0], value];
        //    //}
        //    //// Too many arguments
        //    //else
        //    //{
        //    //    throw new Exception($"Function Language({String.Join(", ", arguments)}) cannot be evaluated for value {value}. It does not support more than one argument.");
        //    //}
        //}
        //private static string[] Slice<T>(T[] array, int index)
        //{
        //    if (index < 0)
        //    {
        //        throw new ArgumentException("Index cannot be negative.", nameof(index));
        //    }
        //    if (index >= array.Length)
        //    {
        //        throw new IndexOutOfRangeException($"Start index {index} is out of range of the array. Array size is {array.Length}.");
        //    }

        //    int length = array.Length - index;
        //    string[] arraySliced = new T[length];
        //    Array.Copy(array, index, arraySliced, 0, length);
        //    return arraySliced;
        //}
    }
}
