﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Localizing
{
    public class LocalizerCache
    {
        public event EventHandler<LocalizerCacheValueChangedEventArgs> ValueChanged;

        private Dictionary<string, object[]> KeyToValues = new Dictionary<string, object[]>();
        internal Localizer Localizer { get; }

        internal LocalizerCache(Localizer localizer)
        {
            Localizer = localizer;
        }

        public bool Has(string key)
        {
            return KeyToValues.ContainsKey(key);
        }
        /// <summary>
        /// Cache substitution values for the specified key.
        /// </summary>
        /// <param name="key">Localization key.</param>
        public object[] Get(string key)
        {
            if (!KeyToValues.TryGetValue(key, out object[] values))
            {
                return new object[0];
            }
            return values;
        }
        /// <summary>
        /// Cache substitution values for the specified key.
        /// </summary>
        /// <param name="key">Localization key.</param>
        /// <param name="values">Values to cache.</param>
        public void Set(string key, params object[] values)
        {
            values = values ?? new object[0];
            KeyToValues[key] = values;
            ValueChanged?.Invoke(this, new LocalizerCacheValueChangedEventArgs(key, values));
        }
        /// <summary>
        /// Remove substitution values for the specified key from cache.
        /// </summary>
        /// <param name="key">Localization key.</param>
        public void Reset(string key)
        {
            KeyToValues.Remove(key);
        }
        /// <summary>
        /// Clear all substitution values from cache.
        /// </summary>
        public void Wipe()
        {
            KeyToValues.Clear();
        }
    }
}
