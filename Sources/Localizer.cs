﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using Iodynis.Libraries.Csving;
using Iodynis.Libraries.Localizing.Internal;

namespace Iodynis.Libraries.Localizing
{
    public class Localizer
    {
        #region Definitions

        /// <summary>
        /// In-memory CSV file representaion.
        /// </summary>
        private Csv Csv { get; }

        private List<string> _Languages;
        public event EventHandler<IReadOnlyList<string>> LanguagesChanged;
        /// <summary>
        /// Available languages.
        /// </summary>
        public IReadOnlyList<string> Languages
        {
            get
            {
                return _Languages;
            }
        }
        public IReadOnlyList<string> Keys
        {
            get
            {
                return Csv.Rows;
            }
        }
        public string KeyColumnName
        {
            get
            {
                return Csv.KeyColumnName;
            }
            set
            {
                Csv.KeyColumnName = value;
            }
        }

        public event EventHandler<Exception> Error;

        public  LocalizerDefaults Defaults { get; private set; }
        public  LocalizerSettings Settings { get; private set; }
        public  LocalizerCache Cache { get; private set; }
        public  LocalizerStorage Storage { get; private set; }
        private LocalizerParser Parser { get; set; }
        #endregion

        #region Constructors
        /// <summary>
        /// Creates a new localizer.
        /// </summary>
        /// <param name="pathOrText">Path to a csv file or csv file content. Path-Content detection is based on \r and \n symbols presence.</param>
        /// <param name="language">Default language to use.</param>
        public Localizer(string pathOrText, string language = null)
        {
            // Read the file
            Csv = new Csv(true, Csv.ReplaceCharacterEnum.ALL);
            Csv.Read(pathOrText);

            if (language != null && !Csv.ColumnExists(language))
            {
                throw new Exception($"Language {language} is not presented.");
            }

            //_Language = language ?? (Csv.Columns.Count > 0 ? Csv.Columns[0] : null);

            Initialize(language ?? (Csv.Columns.Count > 0 ? Csv.Columns[0] : null));
        }
        /// <summary>
        /// Creates a new localizer.
        /// </summary>
        /// <param name="pathOrText">Path to a csv file or csv file content. Path-Content detection is based on \r and \n symbols presence.</param>
        /// <param name="language">Default language to use.</param>
        public Localizer(string pathOrText, int language)
        {
            // Read the file
            Csv = new Csv(true, Csv.ReplaceCharacterEnum.ALL);
            Csv.Read(pathOrText);

            if (language < 0)
            {
                throw new Exception($"Language id {language} should greater or equal to zero.");
            }
            else if (Csv.Columns.Count > language)
            {
                throw new Exception($"Language id {language} should less than language's count of {Csv.Columns.Count}.");
            }

            //_Language = Csv.Columns[language];

            Initialize(Csv.Columns[language]);
        }
        /// <summary>
        /// Creates an empty localizer.
        /// </summary>
        public Localizer()
        {
            // Read the file
            Csv = new Csv(true, Csv.ReplaceCharacterEnum.ALL);

            Initialize();
        }

        private void Initialize(string language = null)
        {
            Defaults = new LocalizerDefaults(this);
            Settings = new LocalizerSettings(this, language);
            Cache = new LocalizerCache(this);
            Storage = new LocalizerStorage(this);
            //Hooker = new LocalizerHooker(this);
            //Walker = new LocalizerWalker(this);
            Parser = new LocalizerParser(this);

            // Common functions
            Defaults.FunctionEnumeration = new LocalizerFunctionEnumeration();
            Defaults.FunctionLowerCase = new LocalizerFunctionLowerCase();
            Defaults.FunctionUpperCase = new LocalizerFunctionUpperCase();
            Defaults.FunctionLanguage = new LocalizerFunctionLanguage();

            Settings.FunctionAdd(Defaults.FunctionEnumeration);
            Settings.FunctionAdd(Defaults.FunctionLowerCase);
            Settings.FunctionAdd(Defaults.FunctionUpperCase);
            Settings.FunctionAdd(Defaults.FunctionLanguage);

            // Specific functions
            Defaults.FunctionPlural = new LocalizerFunctionPlural();
            Defaults.FunctionSex = new LocalizerFunctionEnumeration("Sex");

            Settings.FunctionAdd(Defaults.FunctionPlural);
            Settings.FunctionAdd(Defaults.FunctionSex);

            Settings.FallbackColumnChanged += FallbackKeyChanged;
            Settings.FallbackLanguagesKeyChanged += FallbackLanguagesKeyChanged;

            InitializeLanguages();
        }

        private void InitializeLanguages()
        {
            _Languages = new List<string>(Csv.Columns);
            // Set fallback key column
            if (Settings.FallbackColumn != null && _Languages.Contains(Settings.FallbackColumn))
            {
                _Languages.Remove(Settings.FallbackColumn);
            }

            // Set fallback languages
            if (Settings.FallbackLanguagesKey != null && Keys.Contains(Settings.FallbackLanguagesKey))
            {
                foreach (string language in Languages)
                {
                    string fallback = Csv[language, Settings.FallbackLanguagesKey];
                    if (fallback != null)
                    {
                        Settings.FallbackLanguages[language] = fallback;
                    }
                }
            }

            LanguagesChanged?.Invoke(this, _Languages);
        }
        private void FallbackKeyChanged(object sender, string languageKey)
        {
            _Languages = new List<string>(Csv.Columns);

            // Set fallback key column
            if (Settings.FallbackColumn != null && _Languages.Contains(Settings.FallbackColumn))
            {
                _Languages.Remove(Settings.FallbackColumn);
            }

            LanguagesChanged?.Invoke(this, _Languages);
        }
        private void FallbackLanguagesKeyChanged(object sender, string key)
        {
            Settings.FallbackLanguages.Clear();

            // Set fallback languages
            if (Settings.FallbackLanguagesKey != null && Keys.Contains(Settings.FallbackLanguagesKey))
            {
                foreach (string language in Languages)
                {
                    string fallback = Csv[language, Settings.FallbackLanguagesKey];
                    if (fallback != null)
                    {
                        Settings.FallbackLanguages[language] = fallback;
                    }
                }
            }
        }
        /// <summary>
        /// Loads an additional file or text into the localizer.
        /// </summary>
        /// <param name="pathOrText">Path to a csv file or csv file content. Distinction is based on the \r and \n symbols presence.</param>
        public void Load(string pathOrText)
        {
            Csv.Read(pathOrText);

            // Reinitialize languages
            InitializeLanguages();
        }
        #endregion

        /// <summary>
        /// Add a new language (and a new column in the CSV file).
        /// </summary>
        /// <param name="language">Language to add.</param>
        public void LanguageAdd(string language)
        {
            if (Languages.Contains(language))
            {
                throw new ArgumentException($"The language {language} is already presented.", nameof(language));
            }
            Csv.ColumnAdd(language);
        }
        /// <summary>
        /// Check if the specified language is available.
        /// </summary>
        /// <param name="language">Language to check.</param>
        /// <returns>If the language exists.</returns>
        public bool LanguageExists(string language)
        {
            return Languages.Contains(language);
        }
        public List<string> LanguagesClosest(string language)
        {
            if (!CheckLanguage(language))
            {
                throw new ArgumentException("Language should be of a form xx or xx-XX.", nameof(language));
            }

            List<string> languages = new List<string>();
            (string language2, string language5) = LanguageSplit(language);

            // Long language
            if (language5 != null && Languages.Contains(language5))
            {
                languages.Add(language);
            }
            // Short language
            if (language2 != null && Languages.Contains(language2))
            {
                languages.Add(language2);
            }
            // Long similar languages
            languages.AddRange(Languages.Where(_language =>
                _language != language5 &&
                _language != language2 &&
                _language.StartsWith(language2)));

            return languages;
        }
        public string LanguageClosest(string language, string @default = null)
        {
            if (!CheckLanguage(language))
            {
                throw new ArgumentException("Language should be of a form xx or xx-XX.", nameof(language));
            }

            // Try the exact language
            if (Languages.Contains(language))
            {
                return language;
            }
            // In case of a 'xx-XX' language try the 'xx' language
            string language2 = language.Substring(0, 2);
            if (language.Length == 5 && Languages.Contains(language2))
            {
                return language2;
            }
            // Try to find any languages that starts the same, like 'xx-YY'
            IEnumerable<string> similar = Languages.Where(_language => _language.StartsWith(language2));
            // In case priority key is set, use it to determine the language with the highest priority
            if (Settings.PriorityKey != null)
            {
                return FindMax(similar, _language => LanguagePriority(_language));
            }
            return similar.FirstOrDefault() ?? @default;
        }
        public int LanguagePriority(string language)
        {
            return GetInteger(Settings.PriorityKey, language) ?? 0;
        }
        public string LanguageWithHighestPriority(IEnumerable<string> languages = null)
        {
            return FindMax(languages ?? Languages, _language => LanguagePriority(_language));
        }
        /// <summary>
        /// Remove the specified language (and the corresponding column in the CSV file).
        /// </summary>
        /// <param name="language">Language to remove.</param>
        public void LanguageRemove(string language)
        {
            if (!Languages.Contains(language))
            {
                throw new ArgumentException($"The language {language} is not presented. Only languages presented are {String.Join(", ", Languages)}.", nameof(language));
            }
            Csv.ColumnRemove(language);
        }
        /// <summary>
        /// Checks if the specified key exists (and the corresponding row in the CSV file).
        /// </summary>
        /// <param name="key">Key to check.</param>
        /// <returns>If the key exists.</returns>
        public bool KeyExists(string key)
        {
            return Csv.RowExists(key);
        }
        /// <summary>
        /// Add a new key (and row in the corresponding CSV file).
        /// Values for the key will be null.
        /// Use Set to set the values.
        /// </summary>
        /// <param name="key">Key to add.</param>
        public void KeyAdd(string key)
        {
            Csv.RowAdd(key);
        }
        /// <summary>
        /// Remove the specified key (and the corresponding column in the CSV file).
        /// </summary>
        /// <param name="key">Key to remove.</param>
        public void KeyRemove(string key)
        {
            Csv.RowRemove(key);
        }
        /// <summary>
        /// Get the value of the specified key in the specified language (or the fallback one in case of null).
        /// </summary>
        /// <param name="language">The language.</param>
        /// <param name="key">The key.</param>
        /// <returns>The value.</returns>
        public string GetString(string key, string language)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key), "Key cannot be null.");
            }
            if (key == "")
            {
                throw new ArgumentException("Key cannot be empty.", nameof(key));
            }
            if (language == null)
            {
                throw new ArgumentNullException(nameof(language), "Language cannot be null.");
            }
            if (!Csv.ColumnExists(language))
            {
                throw new ArgumentException($"Language {language} is not presented.", nameof(language));
            }

            // Fallback by language
            string value = GetStringFallbackByLanguage(key, language);

            // Fallback by key
            if (((Settings.FallbackOnNull && value == null) || (Settings.FallbackOnEmpty && value == "") || (Settings.FallbackOnWhitespace && String.IsNullOrWhiteSpace(value))) && Settings.FallbackColumn != null)
            {
                Csv.TryGetValue(Settings.FallbackColumn, key, out string fallbackKey);
                //string fallbackKey = Csv[Settings.FallbackKey, key];
                if (fallbackKey != null)
                {
                    return GetString(fallbackKey, language);
                }
            }

            return value;
        }
        private string GetStringFallbackByLanguage(string key, string language)
        {
            Csv.TryGetValue(language, key, out string value);

            if ((Settings.FallbackOnNull && value == null) || (Settings.FallbackOnEmpty && value == "") || (Settings.FallbackOnWhitespace && String.IsNullOrWhiteSpace(value)))
            {
                if (Settings.FallbackLanguages.TryGetValue(language, out string fallbackLanguage))
                {
                    return GetStringFallbackByLanguage(key, fallbackLanguage);
                }
                if (Settings.FallbackLanguage != null)
                {
                    return GetStringFallbackByLanguage(key, Settings.FallbackLanguage);
                }
            }

            return value;
        }
        public int? GetInteger(string key, string language)
        {
            string @string = GetString(key, language);

            if (Int32.TryParse(@string, out int value))
            {
                return value;
            }

            return null;
        }
        public long? GetLong(string key, string language)
        {
            string @string = GetString(key, language);

            if (Int64.TryParse(@string, out long value))
            {
                return value;
            }

            return null;
        }
        public double? GetDouble(string key, string language)
        {
            string @string = GetString(key, language);

            if (Double.TryParse(@string, out double value))
            {
                return value;
            }

            return null;
        }
        public float? GetSingle(string key, string language)
        {
            string @string = GetString(key, language);

            if (Single.TryParse(@string, out float value))
            {
                return value;
            }

            return null;
        }
        ///// <summary>
        ///// Set the value of the specified key in the specified language.
        ///// </summary>
        ///// <param name="language">The language.</param>
        ///// <param name="key">The key.</param>
        ///// <param name="value">The value.</param>
        //public void Set(string key, string language, string value)
        //{
        //    this[key, language] = value;
        //}
        ///// <summary>
        ///// Set the value of the specified key in the specified language to null.
        ///// </summary>
        ///// <param name="language">The language.</param>
        ///// <param name="key">The key.</param>
        //public void Unset(string key, string language)
        //{
        //    this[key, language] = null;
        //}
        public string Write(IEnumerable<string> keys = null, IEnumerable<string> languages = null)
        {
            return Csv.Print(languages, keys);
        }
        public void Write(string path, IEnumerable<string> keys = null, IEnumerable<string> languages = null)
        {
            Csv.Write(path, languages, keys);
        }
        //#endregion

        #region Localize
        /// <summary>
        /// Get or set the raw value for the specified key using the default language.
        /// </summary>
        /// <param name="key">Localization key.</param>
        /// <returns>Raw localization string.</returns>
        public string this[string key]
        {
            get
            {
                if (!Csv.RowExists(key))
                {
                    throw new Exception($"Key {key} is not presented.");
                }
                return Csv[Settings.Language, key];
            }
            set
            {
                Csv[Settings.Language, key] = value;
            }
        }
        /// <summary>
        /// Get or set the raw value for the specified key using the specified language.
        /// </summary>
        /// <param name="key">Localization key.</param>
        /// <param name="language">Localization language.</param>
        /// <returns>Raw localization string.</returns>
        public string this[string key, string language]
        {
            get
            {
                if (!Csv.RowExists(key))
                {
                    throw new Exception($"Key {key} is not presented.");
                }
                if (!Csv.ColumnExists(language))
                {
                    throw new Exception($"Language {language} is not presented.");
                }
                return Csv[language, key];
            }
            set
            {
                Csv[language, key] = value;
            }
        }

        //private bool IsFirstRun { get; set; } = true;
        ///// <summary>
        ///// Localize all included objects using default language.
        ///// </summary>
        //public void Localize()
        //{
        //    LocalizeTo(_Language);
        //}
        ///// <summary>
        ///// Localize all included objects using the specified language or the default one.
        ///// </summary>
        ///// <param name="language">Language to localize to.</param>
        //public void LocalizeHooked(string language = null)
        //{
        //    Hooker.Localize(language ?? _Language);

        //    //if (IsFirstRun)
        //    //{
        //    //    object[] dictionaryKeys = objectToStringsToLocalize.Keys.ToArray();
        //    //    for (int dictionaryIndex = 0; dictionaryIndex < dictionaryKeys.Length; dictionaryIndex++)
        //    //    {
        //    //        object @object = dictionaryKeys[dictionaryIndex];
        //    //        List<LocalizerAdapterWithDefaultValue> list = objectToStringsToLocalize[@object];
        //    //        for (int listIndex = 0; listIndex < list.Count; listIndex++)
        //    //        {
        //    //            LocalizerAdapterWithDefaultValue adapterWithDefault = list[listIndex];
        //    //            adapterWithDefault.Default = adapterWithDefault.LocalizerAdapter.GetValue<string>(@object);
        //    //            string localized = LocalizeTo(language, adapterWithDefault.Default, new string[0], out int substitutionsCount);
        //    //            if (substitutionsCount != 0)
        //    //            {
        //    //                adapterWithDefault.LocalizerAdapter.SetValue(@object, localized);
        //    //            }
        //    //            else // If nothing was substituted then nothing was localized then nothing will be ever chenged so remove the entry from the list
        //    //            {
        //    //                list.RemoveAt(listIndex--);
        //    //            }
        //    //        }
        //    //        // Remove objects that have nothing localized
        //    //        if (list.Count == 0)
        //    //        {
        //    //            objectToStringsToLocalize.Remove(dictionaryKeys[dictionaryIndex]);
        //    //        }
        //    //    }
        //    //    IsFirstRun = false;
        //    //}
        //    //else
        //    //{
        //    //    foreach (KeyValuePair<object, List<LocalizerAdapterWithDefaultValue>> objectAndAdaptersAndDefaults in objectToStringsToLocalize)
        //    //    {
        //    //        foreach (LocalizerAdapterWithDefaultValue adapterWithDefault in objectAndAdaptersAndDefaults.Value)
        //    //        {
        //    //            if (adapterWithDefault.Default == null)
        //    //            {
        //    //                adapterWithDefault.Default = adapterWithDefault.LocalizerAdapter.GetValue(objectAndAdaptersAndDefaults.Key);
        //    //            }
        //    //            adapterWithDefault.LocalizerAdapter.SetValue(objectAndAdaptersAndDefaults.Key, Localize(adapterWithDefault.Default));
        //    //        }
        //    //    }
        //    //}
        //    //foreach (KeyValuePair<object, List<Tuple<LocalizerAdapter, LocalizerLogic>>> objectAndAdaptersAndLogics in objectToCustoms) foreach (Tuple<LocalizerAdapter, LocalizerLogic> adapterAndLogic in objectAndAdaptersAndLogics.Value)
        //    //{
        //    //    object @object = objectAndAdaptersAndLogics.Key;
        //    //    LocalizerAdapter adapter = adapterAndLogic.Item1;
        //    //    LocalizerLogic logic = adapterAndLogic.Item2;
        //    //    object fieldOrPropertyValue = adapter.GetValue(@object);

        //    //    logic.Localize(language, this, fieldOrPropertyValue);
        //    //}
        //}

        #region Snapshots
        /// <summary>
        /// Get all localizations of the specified key at once.
        /// This is handy when storage variables change and therefore the same key may result in different localizations over time.
        /// <seealso cref="LocalizerString"/> saves the localizations for the specified key for this very moment.
        /// However this method is heavy due to localizing to all available languages at once, so consider careful usage.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns>The object that contains the key localization to all the languages.</returns>
        public LocalizerString LocalizeKeySnapshot(string key, params object[] arguments)
        {
            return LocalizerString.FromKey(this, key, arguments);
        }
        /// <summary>
        /// Get all localizations of the specified string at once.
        /// This is handy when storage variables change and therefore the same string may result in different localizations over time.
        /// <seealso cref="LocalizerString"/> saves the localizations for the specified string for this very moment.
        /// However this method is heavy due to localizing to all available languages at once, so consider careful usage.
        /// </summary>
        /// <param name="string">The string.</param>
        /// <param name="arguments">The arguments.</param>
        /// <returns>The object that contains the string localization to all the languages.</returns>
        public LocalizerString LocalizeStringSnapshot(string @string, params object[] arguments)
        {
            return LocalizerString.FromString(this, @string, arguments);
        }
        #endregion

        /// <summary>
        /// Localize the specified property value using the the default language.
        /// </summary>
        /// <param name="object">The object which property to localize.</param>
        /// <param name="propertyInfo">The property to localize.</param>
        public void LocalizeProperty(object @object, PropertyInfo propertyInfo)
        {
            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }
            if (propertyInfo == null)
            {
                throw new ArgumentNullException(nameof(propertyInfo));
            }
            if (propertyInfo.PropertyType != typeof(string))
            {
                throw new ArgumentException("Only string properties are supported.", nameof(propertyInfo));
            }
            string value = propertyInfo.GetValue(@object)?.ToString();
            if (value != null)
            {
                value = LocalizeString(value);
            }
            propertyInfo.SetValue(@object, value);
        }
        /// <summary>
        /// Localize the specified property value using the the default language.
        /// </summary>
        /// <param name="object">The object which property to localize.</param>
        /// <param name="propertyName">The property name to localize.</param>
        public void LocalizeProperty(object @object, string propertyName)
        {
            if (propertyName == null)
            {
                throw new ArgumentNullException(nameof(propertyName));
            }
            Type type = @object.GetType();
            PropertyInfo propertyInfo = type.GetProperty(propertyName);
            if (propertyInfo == null)
            {
                throw new ArgumentException($"Type {type} does not have a {propertyName} property.", nameof(@object));
            }
            LocalizeProperty(@object, propertyInfo);
        }
        /// <summary>
        /// Localize the specified field value using the the default language.
        /// </summary>
        /// <param name="object">The object which property to localize.</param>
        /// <param name="fieldInfo">The field to localize.</param>
        public void LocalizeField(object @object, FieldInfo fieldInfo)
        {
            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }
            if (fieldInfo == null)
            {
                throw new ArgumentNullException(nameof(fieldInfo));
            }
            if (fieldInfo.FieldType != typeof(string))
            {
                throw new ArgumentException("Only string fields are supported.", nameof(fieldInfo));
            }
            string value = fieldInfo.GetValue(@object)?.ToString();
            if (value != null)
            {
                value = LocalizeString(value);
            }
            fieldInfo.SetValue(@object, value);
        }
        /// <summary>
        /// Localize the specified field value using the the default language.
        /// </summary>
        /// <param name="object">The object which property to localize.</param>
        /// <param name="fieldName">The field name to localize.</param>
        public void LocalizeField(object @object, string fieldName)
        {
            if (fieldName == null)
            {
                throw new ArgumentNullException(nameof(fieldName));
            }
            Type type = @object.GetType();
            FieldInfo fieldInfo = type.GetField(fieldName);
            if (fieldInfo == null)
            {
                throw new ArgumentException($"Type {type} does not have a {fieldName} field.", nameof(@object));
            }
            LocalizeField(@object, fieldInfo);
        }
        /// <summary>
        /// Localize specified key using the default language.
        /// </summary>
        /// <param name="key">Key to localize.</param>
        /// <returns>Localized string.</returns>
        public string LocalizeKey(string key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return LocalizeKey(key, Settings.Language, null, out _);
        }
        /// <summary>
        /// Localize specified key using the default language and substitute the passed values.
        /// </summary>
        /// <param name="key">Key to localize.</param>
        /// <param name="arguments">Values for substitution.</param>
        /// <returns>Localized string.</returns>
        public string LocalizeKey(string key, object[] arguments)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return LocalizeKey(key, Settings.Language, arguments, out _);
        }
        /// <summary>
        /// Localize specified key using the provided language and substitute the passed values.
        /// </summary>
        /// <param name="key">Key to localize.</param>
        /// <param name="arguments">Values for substitution.</param>
        /// <param name="keys">Keys that were substituted.</param>
        /// <returns>Localized string.</returns>
        public string LocalizeKey(string key, object[] arguments, out List<string> keys)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            return LocalizeKey(key, Settings.Language, arguments, out keys);
        }
        /// <summary>
        /// Localize specified key using provided language.
        /// </summary>
        /// <param name="key">Key to localize.</param>
        /// <param name="language">Language to use.</param>
        /// <returns>Localized string.</returns>
        public string LocalizeKey(string key, string language)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (language == null)
            {
                throw new ArgumentNullException(nameof(language));
            }
            return LocalizeKey(key, language, null, out _);
        }
        /// <summary>
        /// Localize specified key using the provided language and substitute the passed values.
        /// </summary>
        /// <param name="key">Key to localize.</param>
        /// <param name="language">Language to use.</param>
        /// <param name="arguments">Values for substitution.</param>
        /// <returns>Localized string.</returns>
        public string LocalizeKey(string key, string language, object[] arguments)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (language == null)
            {
                throw new ArgumentNullException(nameof(language));
            }
            return LocalizeKey(key, language, arguments, out _);
        }
        /// <summary>
        /// Localize specified key using the provided language and substitute the passed values.
        /// </summary>
        /// <param name="key">Key to localize.</param>
        /// <param name="language">Language to use.</param>
        /// <param name="arguments">Values for substitution.</param>
        /// <param name="keys">Keys that were substituted.</param>
        /// <returns>Localized string.</returns>
        public string LocalizeKey(string key, string language, object[] arguments, out List<string> keys)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (language == null)
            {
                throw new ArgumentNullException(nameof(language));
            }
            keys = new List<string>();
            return LocalizeKey(key, language, arguments, keys);
        }
        /// <summary>
        /// Localize specified key using the provided language and substitute the passed values.
        /// </summary>
        /// <param name="key">Key to localize.</param>
        /// <param name="language">Language to use.</param>
        /// <param name="arguments">Values for substitution.</param>
        /// <param name="keys">Keys that were substituted.</param>
        /// <returns>Localized string.</returns>
        public string LocalizeKey(string key, string language, object[] arguments, List<string> keys)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (language == null)
            {
                throw new ArgumentNullException(nameof(language));
            }
            string @string = GetString(key, language);
            return @string != null ? LocalizeString(@string, language, arguments, keys) : @string;
        }
        /// <summary>
        /// Localize specified string using the default language.
        /// </summary>
        /// <param name="string">String to localize.</param>
        /// <returns>Localized string.</returns>
        public string LocalizeString(string @string)
        {
            if (@string == null)
            {
                throw new ArgumentNullException(nameof(@string));
            }
            return LocalizeString(@string, Settings.Language, null, out _);
        }
        /// <summary>
        /// Localize specified string using the default language and substitute the passed values.
        /// </summary>
        /// <param name="string">String to localize.</param>
        /// <param name="arguments">Values for substitution.</param>
        /// <returns>Localized string.</returns>
        public string LocalizeString(string @string, object[] arguments)
        {
            if (@string == null)
            {
                throw new ArgumentNullException(nameof(@string));
            }
            return LocalizeString(@string, Settings.Language, arguments, out _);
        }
        /// <summary>
        /// Localize specified string using the default language and substitute the passed values.
        /// </summary>
        /// <param name="string">String to localize.</param>
        /// <param name="arguments">Values for substitution.</param>
        /// <param name="keys">Keys that were substituted.</param>
        /// <returns>Localized string.</returns>
        public string LocalizeString(string @string, object[] arguments, out List<string> keys)
        {
            if (@string == null)
            {
                throw new ArgumentNullException(nameof(@string));
            }
            return LocalizeString(@string, Settings.Language, arguments, out keys);
        }
        /// <summary>
        /// Localize specified string using the provided language.
        /// </summary>
        /// <param name="string">String to localize.</param>
        /// <param name="language">Language to use.</param>
        /// <returns>Localized string.</returns>
        public string LocalizeString(string @string, string language)
        {
            if (@string == null)
            {
                throw new ArgumentNullException(nameof(@string));
            }
            if (language == null)
            {
                throw new ArgumentNullException(nameof(language));
            }
            return LocalizeString(@string, language, null, out _);
        }
        /// <summary>
        /// Localize specified string using the provided language and substitute the passed values.
        /// </summary>
        /// <param name="string">String to localize.</param>
        /// <param name="language">Language to use.</param>
        /// <param name="arguments">Values for substitution.</param>
        /// <returns>Localized string.</returns>
        public string LocalizeString(string @string, string language, object[] arguments)
        {
            if (@string == null)
            {
                throw new ArgumentNullException(nameof(@string));
            }
            if (language == null)
            {
                throw new ArgumentNullException(nameof(language));
            }
            return LocalizeString(@string, language, arguments, out _);
        }
        /// <summary>
        /// Localize specified string using the provided language and substitute the passed values.
        /// </summary>
        /// <param name="string">String to localize.</param>
        /// <param name="language">Language to use.</param>
        /// <param name="arguments">Values for substitution.</param>
        /// <param name="keys">Keys that were substituted.</param>
        /// <returns>Localized string.</returns>
        public string LocalizeString(string @string, string language, object[] arguments, out List<string> keys)
        {
            if (@string == null)
            {
                throw new ArgumentNullException(nameof(@string));
            }
            if (language == null)
            {
                throw new ArgumentNullException(nameof(language));
            }
            keys = new List<string>();
            return LocalizeString(@string, language, arguments, keys);
        }
        /// <summary>
        /// Localize specified string using the provided language and substitute the passed values.
        /// </summary>
        /// <param name="string">String to localize.</param>
        /// <param name="language">Language to use.</param>
        /// <param name="arguments">Arguments for substitution.</param>
        /// <param name="keys">Keys that were substituted.</param>
        /// <returns>Localized string.</returns>
        private string LocalizeString(string @string, string language, object[] arguments, List<string> keys)
        {
            if (@string == null)
            {
                throw new ArgumentNullException(nameof(@string));
            }
            if (language == null)
            {
                throw new ArgumentNullException(nameof(language));
            }

            //// Do not modify if no language is specified
            //if (language == null)
            //{
            //    return @string;
            //}

            // Check language
            if (!Csv.ColumnExists(language))
            {
                throw new Exception($"Language {language} is not presented.");
            }
            // Empty string
            if (@string == "")
            {
                return "";
            }

            string bra = Settings.Bra;
            string ket = Settings.Ket;

            bool isCharacterEscaped = false;

            StringBuilder stringBuilder = new StringBuilder();
            TextElementEnumerator stringEnumerator = StringInfo.GetTextElementEnumerator(@string);

            while (stringEnumerator.MoveNext())
            {
                string character = stringEnumerator.GetTextElement();

                // Code block
                if (isCharacterEscaped)
                {
                    isCharacterEscaped = false;
                    stringBuilder.Append(character);
                }
                else if (character == "\\")
                {
                    isCharacterEscaped = true;
                }
                else if (character == bra)
                {
                    int count = 1;
                    StringBuilder stringBuilderCode = new StringBuilder();
                    while (stringEnumerator.MoveNext())
                    {
                        character = stringEnumerator.GetTextElement();
                        if (character == bra)
                        {
                            count++;
                            stringBuilderCode.Append(character);
                        }
                        else if (character == ket)
                        {
                            count--;
                            if (count == 0)
                            {
                                break;
                            }
                            else
                            {
                                stringBuilderCode.Append(character);
                            }
                        }
                        else
                        {
                            stringBuilderCode.Append(character);
                        }
                    }

                    try
                    {
                        string code = stringBuilderCode.ToString();
                        arguments = arguments ?? Cache.Get(code);

                        //object[] argumentsCache = Cache.Get(code);
                        //object[] argumentsProvided = arguments;
                        //// Provided more or same amount of arguments compared to ones stored in cache
                        //if (argumentsProvided.Length >= argumentsCache.Length)
                        //{
                        //    arguments = argumentsProvided;
                        //}
                        //else
                        //{
                        //    arguments = argumentsCache;
                        //    Array.Copy(argumentsProvided, 0, arguments, 0, argumentsProvided.Length);
                        //}

                        // Is defined
                        if (Csv.RowExists(code))
                        {
                            keys.Add(code);
                            string key = LocalizeKey(code, language, arguments, keys);
                            stringBuilder.Append(key);
                        }
                        else
                        {
                            //ParserLanguage = language;
                            //ParserArguments = arguments;

                            try
                            {
                                object result = Parser.Parse(code, language, arguments, keys);
                                if (result != null)
                                {
                                    stringBuilder.Append(result.ToString());
                                }
                            }
                            catch (Exception exception)
                            {
                                Error?.Invoke(this, exception);
                                stringBuilder.Append(Settings.ErrorSubstitution ?? code);
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        Error?.Invoke(this, exception);
                    }
                }
                else if (character == ket)
                {
                    throw new Exception("Brakets are unbalanced.");
                }
                else
                {
                    stringBuilder.Append(character);
                }
            }
            if (isCharacterEscaped)
            {
                throw new Exception($"The last character is \\ and it does not escape anything in {@string}.");
            }

            return stringBuilder.ToString();
        }
        #endregion

        #region Util

        private (string language2, string language5) LanguageSplit(string language)
        {
            if (language.Length == 2)
            {
                return (language, null);
            }
            if (language.Length == 5)
            {
                return (language.Substring(0, 2), language.Substring(3, 2));
            }
            throw new ArgumentException("Language length should be 2 or 5.", nameof(language));
        }
        private bool CheckLanguage(string language)
        {
            if (language == null)
            {
                return false;
            }
            if (language.Length == 2)
            {
                return
                    'a' <= language[0] && language[0] <= 'z' &&
                    'a' <= language[1] && language[1] <= 'z';
            }
            if (language.Length == 5)
            {
                return
                    'a' <= language[0] && language[0] <= 'z' &&
                    'a' <= language[1] && language[1] <= 'z' &&
                    language[2] == '-' &&
                    'A' <= language[3] && language[3] <= 'Z' &&
                    'A' <= language[4] && language[4] <= 'Z';
            }
            return false;
        }
        public static T FindMax<T>(IEnumerable<T> enumerable, Func<T, int> measure) where T : class
        {
            T itemMax = default(T);
            int measurementMax = int.MinValue;

            foreach (var item in enumerable)
            {
                int measurement = measure(item);
                if (measurement > measurementMax)
                {
                    itemMax = item;
                    measurementMax = measurement;
                }
            }

            return itemMax;
        }

        #endregion
    }
}
