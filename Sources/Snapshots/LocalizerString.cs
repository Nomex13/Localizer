﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Localizing
{
    public class LocalizerString
    {
        private readonly Localizer Localizer;
        private readonly Dictionary<string, string> Strings = new Dictionary<string, string>();
        private LocalizerString(Localizer localizer, Dictionary<string, string> strings)
        {
            Localizer = localizer;
            Strings = strings;
        }
        public string this[string language]
        {
            get
            {
                return Localize(language);
            }
        }
        private string Localize()
        {
            return Localize(Localizer.Settings.Language);
        }
        private string Localize(string language)
        {
            if (!Strings.TryGetValue(language, out string @string))
            {
                throw new Exception($"Language {language} is not presented.");
            }
            return @string;
        }
        public override string ToString()
        {
            return Localize();
        }

        #region Constructors
        public static LocalizerString FromKey(Localizer localizer, string key, params object[] arguments)
        {
            Dictionary<string, string> strings = new Dictionary<string, string>();
            foreach (string language in localizer.Languages)
            {
                strings[language] = localizer.LocalizeKey(key, language, arguments);
            }
            return new LocalizerString(localizer, strings);
        }
        public static LocalizerString FromString(Localizer localizer, string @string, params object[] arguments)
        {
            Dictionary<string, string> strings = new Dictionary<string, string>();
            foreach (string language in localizer.Languages)
            {
                strings[language] = localizer.LocalizeString(@string, language, arguments);
            }
            return new LocalizerString(localizer, strings);
        }
        #endregion
    }
}
