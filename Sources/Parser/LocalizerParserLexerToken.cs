﻿namespace Iodynis.Libraries.Localizing.Internal
{
    internal enum LocalizerParserLexerToken : int
    {
		ROOT = 1,
		COMMENT,
		COMMENT_XML,
		RETURN,
		NEWLINE,

		ANNOTATION,
		ANNOTATION_NAME,
		ANNOTATION_ARGUMENT,

        COMMENT_XML_TAG,
        COMMENT_XML_TAG_OPENING,
        COMMENT_XML_TAG_CLOSING,

        //OPERATOR_BRACKET_ROUND,
        //OPERATOR_BRACKET_SQUARE,
        //OPERATOR_ASSIGN,
        //OPERATOR_AND,
        //OPERATOR_OR,
        //OPERATOR_EXCLAMATION,

		BRACKET_SQUARE,
		BRA_SQUARE,
		KET_SQUARE,
		BRACKET_ROUND,
		BRA_ROUND,
		KET_ROUND,
		BRACKET_CURLY,
		BRA_CURLY,
		KET_CURLY,
		BRACKET_SHARP,
		BRA_SHARP,
		KET_SHARP,
		BRA_0,
		KET_0,
		BRACKET_0,
		BRA_1,
		KET_1,
		BRACKET_1,
		BRA_2,
		KET_2,
		BRACKET_2,
		BRA_3,
		KET_3,
		BRACKET_3,
		BRA_4,
		KET_4,
		BRACKET_4,
		BRA_5,
		KET_5,
		BRACKET_5,
		BRA_6,
		KET_6,
		BRACKET_6,
		BRA_7,
		KET_7,
		BRACKET_7,
		BRA_8,
		KET_8,
		BRACKET_8,
		BRA_9,
		KET_9,
		BRACKET_9,

		TAB,
		SPACE,
		LETTER,
		DIGIT,
		STRING,
		CHARACTER,
		NUMBER_INTEGER,
		NUMBER_REAL,
		NUMBER_IN_UNITS,
		UNITS,
		NUMBER_HEX,
		NUMBER_OCT,
		NUMBER_DEC,
		NUMBER_BIN,
		INTEGER,
		REAL,
		TIME,
		DATE,

		WORD,
		VALUE,
		NAME,
		LITERAL,

		AND,
		OR,
		NOT,
		XOR,
		INCREMENT,
		DECREMENT,

        /// <summary>
        /// Usually "+"
        /// </summary>
		PLUS,
        /// <summary>
        /// Usually "-"
        /// </summary>
		MINUS,
        /// <summary>
        /// Usually "*"
        /// </summary>
		MULTIPLY,
        /// <summary>
        /// Usually "/"
        /// </summary>
		DIVISION,
        /// <summary>
        /// Usually "%"
        /// </summary>
		REMINDER,

        /// <summary>
        /// Usually "*"
        /// </summary>
		ASTERISK,
        /// <summary>
        /// Usually "/"
        /// </summary>
		SLASH,
        /// <summary>
        /// Usually "="
        /// </summary>
		EQUAL,
		NOT_EQUAL,
        /// <summary>
        /// Usually "&lt;"
        /// </summary>
		LESS,
		LESS_OR_EQUAL,
		NOT_LESS,
        /// <summary>
        /// Usually "&gt;"
        /// </summary>
		GREATER,
		GREATER_OR_EQUAL,
		NOT_GREATER,
		ASSIGN,

        /// <summary>
        /// Usually "!"
        /// </summary>
		EXCLAMATION,
        /// <summary>
        /// Usually "?"
        /// </summary>
		QUESTION,
        /// <summary>
        /// Usually ","
        /// </summary>
		COMMA,
        /// <summary>
        /// Usually "."
        /// </summary>
		DOT,
        /// <summary>
        /// Usually "."
        /// </summary>
		PERIOD,
        /// <summary>
        /// Usually "-"
        /// </summary>
		DASH,
        /// <summary>
        /// Usually "'"
        /// </summary>
		SINGLE_QUOTE,
        /// <summary>
        /// Usually "
        /// </summary>
		DOUBLE_QUOTE,
        /// <summary>
        /// Usually ":"
        /// </summary>
		COLON,
        /// <summary>
        /// Usually ";"
        /// </summary>
		SEMICOLON,

        /// <summary>
        /// Usually "_"
        /// </summary>
		UNDERLINE,
        /// <summary>
        /// Usually "_"
        /// </summary>
		UNDERSCORE,

        /// <summary>
        /// Usually "@"
        /// </summary>
		AT,
        /// <summary>
        /// Usually "#"
        /// </summary>
		SHARP,
        /// <summary>
        /// Usually "$"
        /// </summary>
		DOLLAR,
        /// <summary>
        /// Usually "%"
        /// </summary>
		PERCENTAGE,
        /// <summary>
        /// Usually "^"
        /// </summary>
		CIRCUMFLEX,
        /// <summary>
        /// Usually "|"
        /// </summary>
		BAR,
        /// <summary>
        /// Usually "&"
        /// </summary>
		AMPERSAND,
        /// <summary>
        /// Usually "\"
        /// </summary>
		BACKSLASH,
        /// <summary>
        /// Usually "`"
        /// </summary>
		GRAVE,
        /// <summary>
        /// Usually "~"
        /// </summary>
		TILDE,

		CUSTOM_0,
		CUSTOM_1,
		CUSTOM_2,
		CUSTOM_3,
		CUSTOM_4,
		CUSTOM_5,
		CUSTOM_6,
		CUSTOM_7,
		CUSTOM_8,
		CUSTOM_9,

		ONE,
		TWO,
		THREE,
		MULTIPLE,
		ANY,
		UNKNOWN = 0
	}
}
