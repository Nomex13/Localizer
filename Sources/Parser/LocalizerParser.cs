﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Iodynis.Libraries.Parsing;

namespace Iodynis.Libraries.Localizing.Internal
{
    internal class LocalizerParser
    {
        private Parser Parser { get; set; }
        private string Language;
        private object[] Arguments;
        private List<string> Keys;

        public Localizer Localizer { get; }
        public LocalizerParser(Localizer localizer)
        {
            Localizer = localizer;

            Initialize();
        }

        private void Initialize()
        {
            Lexer lexer = new Lexer()
                .String(LocalizerParserLexerToken.SPACE, " ", "\t")
                .String(LocalizerParserLexerToken.BRA_ROUND, "(")
                .String(LocalizerParserLexerToken.KET_ROUND, ")")
                .String(LocalizerParserLexerToken.BRA_SQUARE, "[")
                .String(LocalizerParserLexerToken.KET_SQUARE, "]")
                .String(LocalizerParserLexerToken.BRA_CURLY, "{")
                .String(LocalizerParserLexerToken.KET_CURLY, "}")
                .String(LocalizerParserLexerToken.COMMA, ",")
                .String(LocalizerParserLexerToken.SHARP, "#")
                .String(LocalizerParserLexerToken.EXCLAMATION, "!")
                .String(LocalizerParserLexerToken.AMPERSAND, "&")
                .String(LocalizerParserLexerToken.BAR, "|")
                .String(LocalizerParserLexerToken.PLUS, "+")
                .String(LocalizerParserLexerToken.MINUS, "-")
                .String(LocalizerParserLexerToken.DIVISION, "/")
                .String(LocalizerParserLexerToken.MULTIPLY, "*")
                .String(LocalizerParserLexerToken.REMINDER, "%")
                .String(LocalizerParserLexerToken.ASSIGN, "≡")
                .String(LocalizerParserLexerToken.EQUAL, "=")
                .String(LocalizerParserLexerToken.LESS, "<")
                .String(LocalizerParserLexerToken.GREATER, ">")
                .String(LocalizerParserLexerToken.LESS_OR_EQUAL, "≤")
                .String(LocalizerParserLexerToken.GREATER_OR_EQUAL, "≥")
                .String(LocalizerParserLexerToken.NOT_EQUAL, "≠")

                //.Regex(LexerToken.CHARACTER, @"'([^\\']|(?:\\')|(?:\\0)|(?:\\a)|(?:\\b)|(?:\\f)|(?:\\v)|(?:\\t)|(?:\\r)|(?:\\n)|(?:\\x[0-9a-fA-F]{1,4})|(?:\\u[0-9a-fA-F]{4})|(?:\\U[0-9a-fA-F]{8}))'")
                .StringBlockEscaped(LocalizerParserLexerToken.STRING, "'", false, "'", false, new Dictionary<string, string>()
                {
                    { "\\\\", "\\" },
                    { "\\\'", "\'" },
                    { "\\t", "\t" },
                    { "\\r", "\r" },
                    { "\\n", "\n" },
                    { "\\0", "\0" },
                    { "\\v", "\v" },
                    { "\\a", "\a" },
                    { "\\b", "\b" },
                    { "\\f", "\f" },
                })

                .Regex(LocalizerParserLexerToken.NUMBER_IN_UNITS, @"([1-9][0-9]*)([a-zA-Z]+)", LocalizerParserLexerToken.NUMBER_REAL, LocalizerParserLexerToken.UNITS)
                .Regex(LocalizerParserLexerToken.LITERAL, @"[a-zA-Z_][a-zA-Z0-9_.]*")
                .Regex(LocalizerParserLexerToken.NUMBER_REAL, @"[0-9]+\.[0-9]+")
                .Regex(LocalizerParserLexerToken.NUMBER_INTEGER, @"[0-9]+")
                .Regex(LocalizerParserLexerToken.NUMBER_HEX, @"0x[0-9]+")
                .Regex(LocalizerParserLexerToken.NUMBER_OCT, @"0[0-9]+")
                .Regex(LocalizerParserLexerToken.NUMBER_BIN, @"0b[0-9]+")
                ;

            Syntaxer syntaxer = new Syntaxer()
                .AddPass("Remove spaces")
                .Remove(LocalizerParserLexerToken.NEWLINE)
                .Remove(LocalizerParserLexerToken.SPACE)
                .AddPass("Group brackets")
                .Group(LocalizerParserLexerToken.BRACKET_ROUND, LocalizerParserLexerToken.BRA_ROUND, LocalizerParserLexerToken.KET_ROUND)
                .Group(LocalizerParserLexerToken.BRACKET_SQUARE, LocalizerParserLexerToken.BRA_SQUARE, LocalizerParserLexerToken.KET_SQUARE)
                .Group(LocalizerParserLexerToken.BRACKET_CURLY, LocalizerParserLexerToken.BRA_CURLY, LocalizerParserLexerToken.KET_CURLY)
                .AddPass("Functions")
                .UnaryLeft(LocalizerParserSyntaxerToken.FUNCTION_ARGUMENT, LocalizerParserLexerToken.SHARP, LocalizerParserLexerToken.NUMBER_INTEGER)
                .Function(LocalizerParserSyntaxerToken.FUNCTION, LocalizerParserSyntaxerToken.ARGUMENT, LocalizerParserLexerToken.LITERAL, LocalizerParserLexerToken.BRACKET_ROUND, LocalizerParserLexerToken.COMMA)
                .AddPass("Operators logical")
                .UnaryLeft(LocalizerParserSyntaxerToken.NOT, LocalizerParserLexerToken.EXCLAMATION)
                .Binary(LocalizerParserSyntaxerToken.AND, LocalizerParserLexerToken.AMPERSAND)
                .Binary(LocalizerParserSyntaxerToken.OR, LocalizerParserLexerToken.BAR)
                .Binary(LocalizerParserSyntaxerToken.XOR, LocalizerParserLexerToken.CIRCUMFLEX)
                .AddPass("Operators multiply/divide")
                .Binary(LocalizerParserSyntaxerToken.MULTIPLY, LocalizerParserLexerToken.MULTIPLY)
                .Binary(LocalizerParserSyntaxerToken.DIVIDE, LocalizerParserLexerToken.DIVISION)
                .Binary(LocalizerParserSyntaxerToken.REMINDER, LocalizerParserLexerToken.REMINDER)
                .AddPass("Operators increment/decrement")
                .ReplaceSequence(LocalizerParserSyntaxerToken.DOUBLEPLUS, LocalizerParserLexerToken.PLUS, LocalizerParserLexerToken.PLUS)
                .UnaryLeft(LocalizerParserSyntaxerToken.PREPLUS, LocalizerParserSyntaxerToken.DOUBLEPLUS)
                .UnaryRight(LocalizerParserSyntaxerToken.POSTPLUS, LocalizerParserSyntaxerToken.DOUBLEPLUS)
                .ReplaceSequence(LocalizerParserSyntaxerToken.DOUBLEMINUS, LocalizerParserLexerToken.MINUS, LocalizerParserLexerToken.MINUS)
                .UnaryLeft(LocalizerParserSyntaxerToken.PREMINUS, LocalizerParserSyntaxerToken.DOUBLEPLUS)
                .UnaryRight(LocalizerParserSyntaxerToken.POSTMINUS, LocalizerParserSyntaxerToken.DOUBLEPLUS)
                .AddPass("Operators plus/minus")
                .Binary(LocalizerParserSyntaxerToken.PLUS, LocalizerParserLexerToken.PLUS)
                .Binary(LocalizerParserSyntaxerToken.MINUS, LocalizerParserLexerToken.MINUS)
                .AddPass("Operators comparison")
                .Binary(LocalizerParserSyntaxerToken.EQUAL, LocalizerParserLexerToken.EQUAL)
                .Binary(LocalizerParserSyntaxerToken.NOT_EQUAL, LocalizerParserLexerToken.NOT_EQUAL)
                .Binary(LocalizerParserSyntaxerToken.LESS, LocalizerParserLexerToken.LESS)
                .Binary(LocalizerParserSyntaxerToken.LESS_OR_EQUAL, LocalizerParserLexerToken.LESS_OR_EQUAL)
                .Binary(LocalizerParserSyntaxerToken.GREATER, LocalizerParserLexerToken.GREATER)
                .Binary(LocalizerParserSyntaxerToken.GREATER_OR_EQUAL, LocalizerParserLexerToken.GREATER_OR_EQUAL)
                //.AddPass("Operators assign")
                //.Binary(SyntaxerToken.ASSIGN, LexerToken.ASSIGN)
                ;

            Interpretator interpretator = new Interpretator()
                .Simple(LocalizerParserLexerToken.LITERAL, (_symbol, _arguments) =>
                {
                    Keys.Add(_symbol);
                    if (Localizer.Storage.TryGet(_symbol, out object valueStored))
                    {
                        return valueStored;
                    }
                    string valueLocalized = Localizer.LocalizeKey(_symbol, Language, Arguments, Keys);
                    return valueLocalized;// ?? _symbol;
                })
                .Simple(LocalizerParserLexerToken.STRING, (_symbol, _arguments) =>
                {
                    return _symbol;
                })
                .Simple(LocalizerParserLexerToken.NUMBER_INTEGER, (_symbol, _arguments) =>
                {
                    return Int32.Parse(_symbol);
                })
                .Simple(LocalizerParserLexerToken.NUMBER_REAL, (_symbol, _arguments) =>
                {
                    return Double.Parse(_symbol);
                })
                .Simple(LocalizerParserSyntaxerToken.EQUAL, (_symbol, _arguments) =>
                {
                    return _arguments[0].ToString() == _arguments[1].ToString();
                })
                .Simple(LocalizerParserSyntaxerToken.NOT_EQUAL, (_symbol, _arguments) =>
                {
                    return _arguments[0].ToString() != _arguments[1].ToString();
                })
                .Simple(LocalizerParserSyntaxerToken.GREATER, (_symbol, _arguments) =>
                {
                    return ToLong(_arguments[0]) > ToLong(_arguments[1]);
                })
                .Simple(LocalizerParserSyntaxerToken.GREATER_OR_EQUAL, (_symbol, _arguments) =>
                {
                    return ToLong(_arguments[0]) >= ToLong(_arguments[1]);
                })
                .Simple(LocalizerParserSyntaxerToken.LESS, (_symbol, _arguments) =>
                {
                    return ToLong(_arguments[0]) < ToLong(_arguments[1]);
                })
                .Simple(LocalizerParserSyntaxerToken.LESS_OR_EQUAL, (_symbol, _arguments) =>
                {
                    return ToLong(_arguments[0]) <= ToLong(_arguments[1]);
                })
                .Simple(LocalizerParserSyntaxerToken.PLUS, (_symbol, _arguments) =>
                {
                    return ToLong(_arguments[0]) + ToLong(_arguments[1]);
                })
                .Simple(LocalizerParserSyntaxerToken.MINUS, (_symbol, _arguments) =>
                {
                    return ToLong(_arguments[0]) - ToLong(_arguments[1]);
                })
                .Simple(LocalizerParserSyntaxerToken.MULTIPLY, (_symbol, _arguments) =>
                {
                    return ToLong(_arguments[0]) * ToLong(_arguments[1]);
                })
                .Simple(LocalizerParserSyntaxerToken.DIVIDE, (_symbol, _arguments) =>
                {
                    return ToLong(_arguments[0]) / ToLong(_arguments[1]);
                })
                .Simple(LocalizerParserSyntaxerToken.REMINDER, (_symbol, _arguments) =>
                {
                    return ToLong(_arguments[0]) % ToLong(_arguments[1]);
                })
                .Simple(LocalizerParserSyntaxerToken.OR, (_symbol, _arguments) =>
                {
                    return ToLong(_arguments[0]) | ToLong(_arguments[1]);
                })
                .Simple(LocalizerParserSyntaxerToken.AND, (_symbol, _arguments) =>
                {
                    return ToLong(_arguments[0]) & ToLong(_arguments[1]);
                })
                .Simple(LocalizerParserSyntaxerToken.XOR, (_symbol, _arguments) =>
                {
                    return ToLong(_arguments[0]) ^ ToLong(_arguments[1]);
                })
                .Simple(LocalizerParserSyntaxerToken.NOT, (_symbol, _arguments) =>
                {
                    return !ToBool(_arguments[0]);
                })
                .Simple(LocalizerParserSyntaxerToken.ARGUMENT, (_symbol, _arguments) =>
                {
                    if (_arguments.Length == 0)
                    {
                        return null;
                    }
                    return _arguments[0];
                })
                .Simple(LocalizerParserSyntaxerToken.FUNCTION_ARGUMENT, (_symbol, _arguments) =>
                {
                    string symbol = _arguments[0].ToString();
                    if (!Int32.TryParse(symbol, out int index))
                    {
                        return null;
                    }
                    if (index < 0 || index >= Arguments.Length)
                    {
                        return null;
                    }
                    return Arguments[index];
                })
                .Simple(LocalizerParserSyntaxerToken.FUNCTION, (_symbol, _arguments) =>
                {
                    LocalizerFunction function = Localizer.Settings.FunctionGet(_symbol);
                    if (function == null)
                    {
                        throw new Exception($"Function {_symbol} is not defined.");
                    }

                    return function.Invoke(Language, _arguments);
                })
                ;

            Parser = new Parser(lexer, syntaxer, interpretator);
            Parser.DebugEvent += OnDebugEvent;
        }
        private static void OnDebugEvent(object sender, ParserDebugEventArguments arguments)
        {
            if (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.BEFORE))
            {
                return;
            }

            Parser parser = sender as Parser;

            string file = "debug." +
                (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.LEXER_PASS) ? "lexer." : "") +
                (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.SYNTAXER_PASS) ? "syntaxer." : "") +
                (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.INTERPRETER_PASS) ? "interpreter." : "") +
                (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.AFTER) ? "after." : "") +
                (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.BEFORE) ? "before." : "") +
                (arguments.PassIndex >= 0 ? ("pass #" + arguments.PassIndex + ".") : "") +
                (String.IsNullOrEmpty(arguments.StageName) ? "" : (arguments.StageName.ToLower().Replace('/', '.').Replace('\\', '.') + ".") ) +
                (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.SUCCESS) ? "success." : "") +
                (arguments.StageType.HasFlag(ParserDebugEventArgumentStageType.FAILURE) ? "failure." : "") +
                "txt";
            File.WriteAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file), parser.TokenManager.PrintSimple(parser, 64), Encoding.UTF8);
        }
        public object Parse(string code, string language, object[] arguments, List<string> keys)
        {
            Language = language;
            Arguments = arguments;
            Keys = keys;

            Initialize();

            return Parser.Parse(code);
        }
        private static long ToLong(object @object)
        {
            switch (@object)
            {
                case bool @bool:
                    return @bool ? 1 : 0;
                case byte @byte:
                    return @byte;
                case char @char:
                    return @char;
                case short @short:
                    return @short;
                case int @int:
                    return @int;
                case long @long:
                    return @long;
                case float @float:
                    return (long)@float;
                case double @double:
                    return (long)@double;
                case string @string:
                    return Int64.Parse(@string);
                default:
                    return Convert.ToInt64(@object);
            }
        }
        private static bool ToBool(object @object)
        {
            switch (@object)
            {
                case bool @bool:
                    return @bool;
                case byte @byte:
                    return @byte != 0;
                case char @char:
                    return @char != 0;
                case short @short:
                    return @short != 0;
                case int @int:
                    return @int != 0;
                case long @long:
                    return @long != 0;
                case float @float:
                    return @float != 0;
                case double @double:
                    return @double != 0;
                case string @string:
                    return @string == "1" || String.Equals(@string, "true", StringComparison.OrdinalIgnoreCase);
                default:
                    return Boolean.Parse(@object.ToString());
            }
        }
    }
}
