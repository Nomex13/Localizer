﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Localizing.Internal
{
    public class LocalizerHook
    {
        public List<string> Keys { get; protected set; }

        public virtual bool IsAlive
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public LocalizerHooker Hooker { get; }
        public Localizer Localizer { get; }

        public LocalizerHook(LocalizerHooker hooker)
        {
            Hooker = hooker;
            Localizer = Hooker.Localizer;
        }
        public void Activate()
        {
            Localize();
        }
        public void Deactivate()
        {
            Delocalize();
        }

        public void Localize(bool rebuildKeys = true)
        {
            Localize(rebuildKeys, Localizer.Settings.Language);
        }
        protected virtual void Localize(bool rebuildKeys, string language, params object[] values)
        {
            throw new NotImplementedException();
        }
        protected virtual void Delocalize()
        {
            throw new NotImplementedException();
        }
    }
}
