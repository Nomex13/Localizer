﻿using System;
using System.Collections.Generic;

namespace Iodynis.Libraries.Localizing.Internal
{
    public class LocalizerHookedObject : LocalizerHook
    {
        public override bool IsAlive => WeakReference.IsAlive;

        private PropertyField PropertyField { get; }
        protected WeakReference WeakReference { get; }
        public string ValueInitial { get; }

        public LocalizerHookedObject(LocalizerHooker hooker, object @object, PropertyField propertyField)
            : base(hooker)
        {
            WeakReference = new WeakReference(@object);
            PropertyField = propertyField;
            ValueInitial = (string)PropertyField.GetValue(@object);
        }

        public bool Is(object @object)
        {
            return WeakReference.Target == @object;
        }
        public bool Is(object @object, string propertyName)
        {
            return WeakReference.Target == @object && PropertyField.Name == propertyName;
        }
        protected override void Localize(bool rebuildKeys, string language, params object[] values)
        {
            object @object = WeakReference.Target;
            if (!WeakReference.IsAlive)
            {
                return;
            }
            // Get localized value
            string valueLocalized = Localizer.LocalizeString(ValueInitial, language, values, out List<string> keys);
            PropertyField.SetValue(@object, valueLocalized);

            // Hook onto replaced keys, so the hooker will automatically call localize again if they are changed
            if (Keys != null)
            {
                Hooker.FreeKeys(this);
            }
            //if (Keys == null || rebuildKeys)
            {
                Keys = keys;
                Hooker.UseKeys(this);
            }
        }
        protected override void Delocalize()
        {
            if (Keys != null)
            {
                Hooker.UseKeys(this);
            }
            Keys = null;

            object @object = WeakReference.Target;
            if (!WeakReference.IsAlive)
            {
                return;
            }

            // Get initial value
            PropertyField.SetValue(@object, ValueInitial);
        }
    }
}
