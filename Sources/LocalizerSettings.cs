﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Iodynis.Libraries.Localizing
{
    public class LocalizerSettings
    {
        private string _Bra = "{";
        /// <summary>
        /// Key open bracket.
        /// Default is "{".
        /// </summary>
        public string Bra
        {
            get
            {
                return _Bra;
            }
            set
            {
                if (value == null)
                {
                    throw new Exception("Open bracket cannot be null.");
                }
                if (value.Length == 0)
                {
                    throw new Exception("Open bracket cannot be empty.");
                }
                StringInfo stringInfo = new StringInfo(value);
                if (stringInfo.LengthInTextElements != 1)
                {
                    throw new Exception("Open bracket length should be 1.");
                }
                _Bra = value;
            }
        }
        private string _Ket = "}";
        /// <summary>
        /// Key close bracket.
        /// Default is "}".
        /// </summary>
        public string Ket
        {
            get
            {
                return _Ket;
            }
            set
            {
                if (value == null)
                {
                    throw new Exception("Close bracket cannot be null.");
                }
                if (value.Length == 0)
                {
                    throw new Exception("Close bracket cannot be empty.");
                }
                StringInfo stringInfo = new StringInfo(value);
                if (stringInfo.LengthInTextElements != 1)
                {
                    throw new Exception("Close bracket length should be 1.");
                }
                _Ket = value;
            }
        }
        ///// <summary>
        ///// Delimiter between the key and the function.
        ///// Default is ":".
        ///// </summary>
        //public string FunctionDelimiter { get; set; } = ":";
        ///// <summary>
        ///// Delimiter between the function arguments.
        ///// Default is ",".
        ///// </summary>
        //public string ArgumentDelimiter { get; set; } = ",";

        private string _Language;
        private string _LanguageLowerCase;
        private string _LanguageUpperCase;
        public event EventHandler<string> LanguageChanged;
        /// <summary>
        /// Language to use if it is not specified explicitly.
        /// </summary>
        public string Language
        {
            get
            {
                return _Language;
            }
            set
            {
                if (!Localizer.Languages.Contains(value))
                {
                    throw new Exception($"Language {value} is not presented.");
                }
                _Language = value;
                _LanguageLowerCase = value.ToLowerInvariant();
                _LanguageUpperCase = value.ToUpperInvariant();
                LanguageChanged?.Invoke(this, _Language);
            }
        }
        /// <summary>
        /// By default, if an exception occurs during localization, then the original string is returned.
        /// If <see cref="ErrorSubstitution"/> is not null, then its value is returned instead.
        /// </summary>
        public string ErrorSubstitution = null;
        /// <summary>
        /// Key under which language priorities are stored.
        /// For each language its priority should be an integer or empty.
        /// The language with the highest priority is set to be the default one.
        /// Also, priority determines which language to use if the exact language is not presented and there are several languages that share the same 2-symbol code.
        /// </summary>
        public string PriorityKey { get; set; }
        /// <summary>
        /// Functions.
        /// The usage is {Function(Argument1, Argumnt2)}
        /// </summary>
        private Dictionary<string, LocalizerFunction> Functions { get; } = new Dictionary<string, LocalizerFunction>();

        public event EventHandler<string> FallbackLanguagesKeyChanged;
        private string _FallbackLanguagesKey;
        /// <summary>
        /// Key to be used to fill <see cref="FallbackLanguages"/> when reading a CSV file.
        /// Set it to the key that for every language contains a fallback language or null.
        /// </summary>
        public string FallbackLanguagesKey
        {
            get
            {
                return _FallbackLanguagesKey;
            }
            set
            {
                if (_FallbackLanguagesKey == value)
                {
                    return;
                }
                if (!Localizer.KeyExists(value))
                {
                    throw new Exception($"Fallback languages key {value} is not presented among the keys.");
                }

                _FallbackLanguagesKey = value;
                FallbackLanguagesKeyChanged?.Invoke(this, value);
            }
        }
        public Dictionary<string, string> FallbackLanguages { get; } = new Dictionary<string, string>();
        /// <summary>
        /// Language to try if translation for the original language is null (or empty, depends on settings).
        /// Set it to a general language to try after fallback to more specific languages in <see cref="FallbackLanguages"/> fail.
        /// </summary>
        public string FallbackLanguage { get; set; }

        private string _FallbackColumn;
        public event EventHandler<string> FallbackColumnChanged;
        /// <summary>
        /// Column name for keys to try if translations for the original language, specific fallback languages <see cref="FallbackLanguages"/> and general fallback language <see cref="FallbackLanguage"/> fail.
        /// Set it to the name of the column that for every key contains not a translated value, but a new key to try if translation fails.
        /// The new key will be tried if the value for the originaly requested key is null (or empty, depends on settings).
        /// </summary>
        public string FallbackColumn
        {
            get
            {
                return _FallbackColumn;
            }
            set
            {
                if (_FallbackColumn == value)
                {
                    return;
                }
                if (!Localizer.LanguageExists(value))
                {
                    throw new Exception($"Fallback column {value} is not presented. Only {String.Join(", ", Localizer.Languages)}{(FallbackColumn == null && Localizer.Languages.Count > 0 ? "" : $", {FallbackColumn}")} columns are presented.");
                }

                _FallbackColumn = value;
                FallbackColumnChanged?.Invoke(this, value);
            }
        }
        /// <summary>
        /// If the key is translated into a null string then the corresponding fallback language will be tried.
        /// </summary>
        public bool FallbackOnNull { get; set; } = true;
        /// <summary>
        /// If the key is translated into an empty string then the corresponding fallback language will be tried.
        /// </summary>
        public bool FallbackOnEmpty { get; set; } = false;
        /// <summary>
        /// If the key is translated into a whitespace string then the corresponding fallback language will be tried.
        /// </summary>
        public bool FallbackOnWhitespace { get; set; } = false;
        /// <summary>
        /// If the key is not found it will be removed from the localized string.
        /// </summary>
        public bool RemoveNonLocalizedKeys { get; set; } = false;

        internal Localizer Localizer { get; }
        internal LocalizerSettings(Localizer localizer, string language)
        {
            Localizer = localizer;
            _Language = language;
        }

        #region Functions
        /// <summary>
        /// Checks if the specified key corresponds to any function.
        /// </summary>
        /// <param name="key">The key to check.</param>
        /// <returns>If there is a function that corresponds to the specified key.</returns>
        public bool FunctionExists(string key)
        {
            return Functions.ContainsKey(key);
        }
        /// <summary>
        /// Get the function that corresponds to the specified key.
        /// </summary>
        /// <param name="key">The key.</param>
        /// <returns>The function that corresponds to the specified key.</returns>
        public LocalizerFunction FunctionGet(string key)
        {
            Functions.TryGetValue(key, out LocalizerFunction function);
            return function;
        }
        /// <summary>
        /// Add a new function.
        /// </summary>
        /// <param name="function">The function.</param>
        public void FunctionAdd(LocalizerFunction function)
        {
            if (Functions.ContainsKey(function.Name))
            {
                throw new Exception($"Function with the key {function.Name} is already presented.");
            }
            if (function.Localizer != null)
            {
                throw new Exception($"Function with the key {function.Name} is set to work with another localizer instance. Functions cannot be shared between different localizers.");
            }
            function.Localizer = Localizer;
            Functions.Add(function.Name, function);
        }
        /// <summary>
        /// Set a function.
        /// </summary>
        /// <param name="key">The key to trigger the function.</param>
        /// <param name="function">The function.</param>
        public void FunctionSet(string key, LocalizerFunction function)
        {
            if (function.Localizer != null)
            {
                throw new Exception($"Function with the key {key} is set to work with another localizer instance. Functions cannot be shared between different localizers.");
            }
            function.Localizer = Localizer;
            Functions[key] = function;
        }
        /// <summary>
        /// Remove an existing function.
        /// </summary>
        /// <param name="key">The key of the function to remove.</param>
        public void FunctionRemove(string key)
        {
            if (!Functions.ContainsKey(key))
            {
                throw new Exception($"Function with the key {key} is not presented.");
            }
            Functions.Remove(key);
        }
        #endregion
    }
}
