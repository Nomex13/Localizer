﻿using System;
using System.Linq.Expressions;

namespace Iodynis.Libraries.Localizing
{
    internal class Util
    {
        /// <summary>
        /// Compiles a property getter.
        /// </summary>
        /// <typeparam name="TObject">Object type.</typeparam>
        /// <typeparam name="TProperty">Property type.</typeparam>
        /// <param name="propertyName">Property name.</param>
        /// <returns>Property getter.</returns>
        public static Func<TObject, TProperty> GetPropertyGetter<TObject, TProperty>(string propertyName)
        {
            ParameterExpression parameterObject = Expression.Parameter(typeof(TObject), "value");
            Expression expression = Expression.Property(parameterObject, propertyName);
            Func<TObject, TProperty> getter = Expression.Lambda<Func<TObject, TProperty>>(expression, parameterObject).Compile();

            return getter;
        }
        /// <summary>
        /// Compiles a property setter.
        /// </summary>
        /// <typeparam name="TObject">Object type.</typeparam>
        /// <typeparam name="TProperty">Property type.</typeparam>
        /// <param name="propertyName">Property name.</param>
        /// <returns>Property setter.</returns>
        public static Action<TObject, TProperty> GetPropertySetter<TObject, TProperty>(string propertyName)
        {
            ParameterExpression parameterObject = Expression.Parameter(typeof(TObject));
            ParameterExpression parameterPropertyValue = Expression.Parameter(typeof(TProperty), propertyName);
            MemberExpression expression = Expression.Property(parameterObject, propertyName);
            Action<TObject, TProperty> setter = Expression.Lambda<Action<TObject, TProperty>>
            (
                Expression.Assign(expression, parameterPropertyValue), parameterObject, parameterPropertyValue
            ).Compile();

            return setter;
        }
    }
}
