﻿//using System;
//using System.Reflection;
//using Iodynis.Libraries.Localizing.Internal;

//namespace Iodynis.Libraries.Localizing
//{
//    public class LocalizerWalkerRulePropertyExclude : LocalizerWalkerRule
//    {
//        private PropertyInfo PropertyInfo;
//        public LocalizerWalkerRulePropertyExclude(PropertyInfo propertyInfo)
//        {
//            PropertyInfo = propertyInfo;
//            Stop = true;
//        }
//        public LocalizerWalkerRulePropertyExclude(Type type, string name)
//        {
//            PropertyInfo = type.GetProperty(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
//            Stop = true;
//        }
//        public override bool Match(Type type)
//        {
//            return PropertyInfo.DeclaringType == type;
//        }
//    }

//    public static partial class Extensions
//    {
//        public static void Exclude(this LocalizerWalker walker, PropertyInfo propertyInfo)
//        {
//            walker.RuleAdd(new LocalizerWalkerRulePropertyExclude(propertyInfo));
//        }
//        public static void Exclude(this LocalizerWalker walker, Type type, string name)
//        {
//            walker.RuleAdd(new LocalizerWalkerRulePropertyExclude(type, name));
//        }
//    }
//}
