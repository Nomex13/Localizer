﻿//using System;
//using System.Collections;
//using System.Collections.Generic;

//namespace Iodynis.Libraries.Localizing.Internal

//{
//    public class LocalizerWalkerLogicForIList : LocalizerWalkerLogic
//    {
//        private List<string> defaults;
//        private List<object> objects;

//        public LocalizerWalkerLogicForIList()
//            : base(typeof(IList))
//        {
//            //_Solo = true;
//        }

//        public override void Localize(string language, Localizer localizer, object @object)
//        {
//            IList values = (IList)@object;

//            // At first run need to store the initial values of every item that is a string.
//            // Additionally store all objects that are not strings into a separate list.
//            if (defaults == null)
//            {
//                defaults = new List<string>(values.Count);
//                objects = new List<object>();
//                foreach (object value in values)
//                {
//                    // Save nulls at their specific indexes
//                    if (value == null)
//                    {
//                        defaults.Add(null);
//                    }
//                    // Save strings at their specific indexes
//                    else if (value is string)
//                    {
//                        defaults.Add((string)value);
//                    }
//                    // Save objects in the list
//                    else
//                    {
//                        defaults.Add(null);
//                        objects.Add(value);
//                    }
//                }
//                for (int index = 0; index < values.Count; index++)
//                {
//                    // Set only those strings that were initially set and were localizable
//                    if (defaults[index] != null)
//                    {
//                        string valueLocalized = localizer.LocalizeString(defaults[index], language, new string[0], out List<string> keys);
//                        if (keys.Count != 0)
//                        {
//                            values[index] = valueLocalized;
//                        }
//                        else // If no keys were found then nothing was substituted then nothing was localized then nothing will be ever chenged so nullize the entry in the list
//                        {
//                            defaults[index] = null;
//                        }
//                    }
//                }
//            }
//            // Ensure that lengths of the lists are in sync and can safely be iterated simultaneously
//            else if (defaults.Count != values.Count)
//            {
//                throw new Exception($"IList items count has changed from initial {defaults.Count} to current {values.Count}. Such changes are not supported by Localizer automatic mode. Please use manual localization instead.");
//            }
//            // Safe to do the localization now
//            else
//            {
//                for (int index = 0; index < values.Count; index++)
//                {
//                    // Set only those strings that were initially set and were localizable
//                    if (defaults[index] != null)
//                    {
//                        values[index] = localizer.LocalizeString(defaults[index], language);
//                    }
//                }
//            }
//        }
//        public override IEnumerable Walk(object @object)
//        {
//            return objects;
//        }
//        public override LocalizerWalkerLogic Clone()
//        {
//            return new LocalizerWalkerLogicForIList();
//        }
//    }
//}
