﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Iodynis.Libraries.Localizing
{
    public class LocalizerWalkerTypeInfoAssembler
    {
        private HashSet<Type>   Types = new HashSet<Type>();
        private HashSet<string> Names = new HashSet<string>();
        private HashSet<Func<object, object>> RuntimeObjects = new HashSet<Func<object, object>>();
        private HashSet<Func<object, IEnumerable<object>>> RuntimeEnumerables = new HashSet<Func<object, IEnumerable<object>>>();

        public LocalizerWalkerTypeInfoAssembler()
        {
            ;
        }

        public void Add(IEnumerable<Type> types)
        {
            foreach (Type type in types)
            {
                Types.Add(type);
            }
        }
        public void Add(IEnumerable<string> names)
        {
            foreach (string name in names)
            {
                Names.Add(name);
            }
        }
        public void Add(Func<object, object> function)
        {
            RuntimeObjects.Add(function);
        }
        public void Add(Func<object, IEnumerable<object>> function)
        {
            RuntimeEnumerables.Add(function);
        }

        public void Fill(Type type, List<Func<object, object>> runtimeObjects, List<Func<object, IEnumerable<object>>> runtimeEnumerables)
        {
            runtimeObjects.AddRange(RuntimeObjects);
            runtimeEnumerables.AddRange(RuntimeEnumerables);
        }
        public void Fill(Type type, List<FieldInfo> fieldInfos, List<PropertyInfo> propertyInfos)
        {
            foreach (string name in Names)
            {
                FieldInfo fieldInfo = type.GetField(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                if (fieldInfo != null)
                {
                    fieldInfos.Add(fieldInfo);
                }
                else
                {
                    PropertyInfo propertyInfo = type.GetProperty(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
                    if (propertyInfo != null)
                    {
                        propertyInfos.Add(propertyInfo);
                    }
                    else
                    {
                        throw new ArgumentException(nameof(name), $"The type {type} has neither property nor field named {name}.");
                    }
                }
            }

            fieldInfos.AddRange(type
                .GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
                .Where(_fieldInfo => GetTypes(_fieldInfo.FieldType).Any(_type => Types.Contains(_type))));

            propertyInfos.AddRange(type
                .GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic)
                .Where(_propertyInfo => GetTypes(_propertyInfo.PropertyType).Any(_type => Types.Contains(_type))));
        }
        private static IEnumerable<Type> GetTypes(Type type)
        {
            Type baseType = type;
            while (baseType != null)
            {
                yield return baseType;
                baseType = baseType.BaseType;
            }
            foreach (Type @interface in type.GetInterfaces())
            {
                yield return @interface;
            }
        }
    }
}
