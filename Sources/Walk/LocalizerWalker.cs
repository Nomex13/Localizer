﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Iodynis.Libraries.Localizing.Internal;

namespace Iodynis.Libraries.Localizing
{
    public class LocalizerWalker
    {
        private bool IsInitialized = false;

        private readonly Dictionary<Type, LocalizerWalkerTypeInfoAssembler> TypeToWalkInfoAssembler     = new Dictionary<Type, LocalizerWalkerTypeInfoAssembler>();
        private readonly Dictionary<Type, LocalizerWalkerTypeInfoAssembler> TypeToLocalizeInfoAssembler = new Dictionary<Type, LocalizerWalkerTypeInfoAssembler>();

        // Results are stored into info objects
        private readonly Dictionary<Type, LocalizerWalkerTypeInfo> TypeToInfo = new Dictionary<Type, LocalizerWalkerTypeInfo>();

        // Additional pre-filters
        private readonly HashSet<object> ObjectsExcluded = new HashSet<object>();
        private readonly HashSet<Type> TypesExcluded = new HashSet<Type>();

        internal Localizer Localizer { get; }
        /// <summary>
        /// Instantiate a walker that will analyze object properties and references.
        /// It will localize whole object structures based on rules.
        /// </summary>
        /// <param name="localizer">The localizer to use when performing the localization.</param>
        public LocalizerWalker(Localizer localizer)
        {
            Localizer = localizer;
        }

        /// <summary>
        /// Set the specified type fields and properties up for localization.
        /// </summary>
        /// <param name="type">The type to set up.</param>
        /// <param name="propertyAndFieldNames">Names of fields and properties to mark for localization.</param>
        public void Localize(Type type, params string[] propertyAndFieldNames)
        {
            if (!TypeToLocalizeInfoAssembler.TryGetValue(type, out LocalizerWalkerTypeInfoAssembler maker))
            {
                maker = new LocalizerWalkerTypeInfoAssembler();
                TypeToLocalizeInfoAssembler.Add(type, maker);
            }
            maker.Add(propertyAndFieldNames);
        }
        /// <summary>
        /// Set the specified type fields and properties up for localization.
        /// </summary>
        /// <param name="propertyAndFieldNames">Names of fields and properties to mark for localization.</param>
        public void Localize<T>(params string[] propertyAndFieldNames)
        {
            if (!TypeToLocalizeInfoAssembler.TryGetValue(typeof(T), out LocalizerWalkerTypeInfoAssembler maker))
            {
                maker = new LocalizerWalkerTypeInfoAssembler();
                TypeToLocalizeInfoAssembler.Add(typeof(T), maker);
            }
            maker.Add(propertyAndFieldNames);
        }
        public void Walk(Type type, params string[] propertyAndFieldNames)
        {
            if (!TypeToWalkInfoAssembler.TryGetValue(type, out LocalizerWalkerTypeInfoAssembler maker))
            {
                maker = new LocalizerWalkerTypeInfoAssembler();
                TypeToWalkInfoAssembler.Add(type, maker);
            }
            maker.Add(propertyAndFieldNames);
        }
        public void Walk<T>(params string[] propertyAndFieldNames)
        {
            if (!TypeToWalkInfoAssembler.TryGetValue(typeof(T), out LocalizerWalkerTypeInfoAssembler maker))
            {
                maker = new LocalizerWalkerTypeInfoAssembler();
                TypeToWalkInfoAssembler.Add(typeof(T), maker);
            }
            maker.Add(propertyAndFieldNames);
        }
        public void Walk(Type type, params Type[] propertyAndFieldTypes)
        {
            if (!TypeToWalkInfoAssembler.TryGetValue(type, out LocalizerWalkerTypeInfoAssembler maker))
            {
                maker = new LocalizerWalkerTypeInfoAssembler();
                TypeToWalkInfoAssembler.Add(type, maker);
            }
            maker.Add(propertyAndFieldTypes);
        }
        public void Walk<T>(params Type[] propertyAndFieldTypes)
        {
            if (!TypeToWalkInfoAssembler.TryGetValue(typeof(T), out LocalizerWalkerTypeInfoAssembler maker))
            {
                maker = new LocalizerWalkerTypeInfoAssembler();
                TypeToWalkInfoAssembler.Add(typeof(T), maker);
            }
            maker.Add(propertyAndFieldTypes);
        }
        public void Walk(Type type, Func<object, object> walker)
        {
            if (!TypeToWalkInfoAssembler.TryGetValue(type, out LocalizerWalkerTypeInfoAssembler maker))
            {
                maker = new LocalizerWalkerTypeInfoAssembler();
                TypeToWalkInfoAssembler.Add(type, maker);
            }
            maker.Add(walker);
        }
        public void Walk<T>(Func<T, object> walker)
        {
            if (!TypeToWalkInfoAssembler.TryGetValue(typeof(T), out LocalizerWalkerTypeInfoAssembler maker))
            {
                maker = new LocalizerWalkerTypeInfoAssembler();
                TypeToWalkInfoAssembler.Add(typeof(T), maker);
            }
            maker.Add((_object) => walker((T)_object));
        }
        public void Walk(Type type, Func<object, IEnumerable<object>> walker)
        {
            if (!TypeToWalkInfoAssembler.TryGetValue(type, out LocalizerWalkerTypeInfoAssembler maker))
            {
                maker = new LocalizerWalkerTypeInfoAssembler();
                TypeToWalkInfoAssembler.Add(type, maker);
            }
            maker.Add(walker);
        }
        public void Walk<T>(Func<T, IEnumerable<object>> walker)
        {
            if (!TypeToWalkInfoAssembler.TryGetValue(typeof(T), out LocalizerWalkerTypeInfoAssembler maker))
            {
                maker = new LocalizerWalkerTypeInfoAssembler();
                TypeToWalkInfoAssembler.Add(typeof(T), maker);
            }
            maker.Add((_object) => walker((T)_object));
        }

        public void WalkAndLocalize(object @object)
        {
            if (!IsInitialized)
            {
                throw new Exception("Walker is not yet initialized.");
            }
            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }

            LocalizeRecursive(@object, new HashSet<object>());
        }
        private void LocalizeRecursive(object @object, HashSet<object> walked)
        {
            // Check if the object is excluded
            if (ObjectsExcluded.Contains(@object))
            {
                return;
            }

            // Check if the type is excluded
            Type type = @object.GetType();
            if (TypesExcluded.Contains(@object))
            {
                return;
            }

            if (!TypeToInfo.TryGetValue(type, out LocalizerWalkerTypeInfo info))
            {
                // Gather info for the new type
                info = GatherInfo(type);
                // Exclude the type
                if (info == null)
                {
                    TypesExcluded.Add(type);
                    return;
                }
                TypeToInfo.Add(type, info);
            }

            // Check for a loop
            if (walked.Contains(@object))
            {
                return;
            }
            // Mark as walked
            else
            {
                walked.Add(@object);
            }

            // Localize properties
            foreach (PropertyInfo propertyInfo in info.LocalizeProperties)
            {
                Localizer.LocalizeProperty(@object, propertyInfo);
            }
            // Localize fields
            foreach (FieldInfo fieldInfo in info.LocalizeFields)
            {
                Localizer.LocalizeField(@object, fieldInfo);
            }

            // Walk properties
            foreach (PropertyInfo propertyInfo in info.WalkProperties)
            {
                object value = propertyInfo.GetValue(@object);
                if (value != null)
                {
                    LocalizeRecursive(value, walked);
                }
            }
            // Walk fields
            foreach (FieldInfo fieldInfo in info.WalkFields)
            {
                object value = fieldInfo.GetValue(@object);
                if (value != null)
                {
                    LocalizeRecursive(value, walked);
                }
            }
            // Walk objects
            foreach (Func<object, object> getter in info.RuntimeObjects)
            {
                object runtimeObject = getter(@object);
                if (runtimeObject != null)
                {
                    LocalizeRecursive(runtimeObject, walked);
                }
            }
            foreach (Func<object, IEnumerable<object>> getter in info.RuntimeEnumerables)
            {
                IEnumerable<object> runtimeEnumerable = getter(@object);
                if (runtimeEnumerable != null)
                {
                    foreach (object runtimeObject in runtimeEnumerable)
                    {
                        if (runtimeObject != null)
                        {
                            LocalizeRecursive(runtimeObject, walked);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Walk through the object structure and hook to all the fields and properties based on the rules previously set.
        /// </summary>
        /// <param name="object">The root object of the object structure.</param>
        /// <returns>A new instance of <see cref="LocalizerHooker"/> that has got all the fields and properties hooked and ready for localization.</returns>
        public LocalizerHooker WalkAndHook(object @object)
        {
            if (!IsInitialized)
            {
                throw new Exception("Walker is not yet initialized.");
            }
            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }

            LocalizerHooker hooker = new LocalizerHooker(Localizer);
            HookRecursive(@object, hooker, new HashSet<object>());
            return hooker;
        }
        /// <summary>
        /// Walk through the object structure and hook to all the fields and properties based on the rules previously set.
        /// </summary>
        /// <param name="object">The root object of the object structure.</param>
        /// <param name="hooker">An existing instance of <see cref="LocalizerHooker"/> to add all the new field and property hooks to.</param>
        public void WalkAndHook(object @object, LocalizerHooker hooker)
        {
            if (!IsInitialized)
            {
                throw new Exception("Walker is not yet initialized.");
            }
            if (@object == null)
            {
                throw new ArgumentNullException(nameof(@object));
            }
            if (hooker == null)
            {
                throw new ArgumentNullException(nameof(hooker));
            }

            HookRecursive(@object, hooker, new HashSet<object>());
        }
        private void HookRecursive(object @object, LocalizerHooker hooker, HashSet<object> walked)
        {
            // Check if the object is excluded
            if (ObjectsExcluded.Contains(@object))
            {
                return;
            }

            // Check if the type is excluded
            Type type = @object.GetType();
            if (TypesExcluded.Contains(@object))
            {
                return;
            }

            if (!TypeToInfo.TryGetValue(type, out LocalizerWalkerTypeInfo info))
            {
                // Gather info for the new type
                info = GatherInfo(type);
                // Exclude the type
                if (info == null)
                {
                    TypesExcluded.Add(type);
                    return;
                }
                TypeToInfo.Add(type, info);
            }

            // Check for a loop
            if (walked.Contains(@object))
            {
                return;
            }
            // Mark as walked
            else
            {
                walked.Add(@object);
            }

            // Hook properties
            foreach (PropertyInfo propertyInfo in info.LocalizeProperties)
            {
                hooker.Hook(@object, propertyInfo);
            }
            // Hook fields
            foreach (FieldInfo fieldInfo in info.LocalizeFields)
            {
                hooker.Hook(@object, fieldInfo);
            }

            // Walk properties
            foreach (PropertyInfo propertyInfo in info.WalkProperties)
            {
                object value = propertyInfo.GetValue(@object);
                if (value != null)
                {
                    HookRecursive(value, hooker, walked);
                }
            }
            // Walk fields
            foreach (FieldInfo fieldInfo in info.WalkFields)
            {
                object value = fieldInfo.GetValue(@object);
                if (value != null)
                {
                    HookRecursive(value, hooker, walked);
                }
            }
            // Walk objects
            foreach (Func<object, object> getter in info.RuntimeObjects)
            {
                object runtimeObject = getter(@object);
                if (runtimeObject != null)
                {
                    HookRecursive(runtimeObject, hooker, walked);
                }
            }
            foreach (Func<object, IEnumerable<object>> getter in info.RuntimeEnumerables)
            {
                IEnumerable<object> runtimeEnumerable = getter(@object);
                if (runtimeEnumerable != null)
                {
                    foreach (object runtimeObject in runtimeEnumerable)
                    {
                        if (runtimeObject != null)
                        {
                            HookRecursive(runtimeObject, hooker, walked);
                        }
                    }
                }
            }
        }
        public void Initialize()
        {
            if (IsInitialized)
            {
                throw new Exception("Walker is already initialized.");
            }

            IsInitialized = true;
        }
        private LocalizerWalkerTypeInfo GatherInfo(Type type)
        {
            LocalizerWalkerTypeInfo info = new LocalizerWalkerTypeInfo(type);

            // Get all types inherited by the provided type as well as all implemented interfaces
            List<Type> inheritedTypes = GetInheritedTypesAndInterfaces(type);
            foreach (Type inheritedType in inheritedTypes)
            {
                if (TypeToWalkInfoAssembler.TryGetValue(inheritedType, out LocalizerWalkerTypeInfoAssembler walkInfoAssembler))
                {
                    walkInfoAssembler.Fill(type, info.WalkFields, info.WalkProperties);
                    walkInfoAssembler.Fill(type, info.RuntimeObjects, info.RuntimeEnumerables);
                }
                if (TypeToLocalizeInfoAssembler.TryGetValue(inheritedType, out LocalizerWalkerTypeInfoAssembler localizeInfoAssembler))
                {
                    localizeInfoAssembler.Fill(type, info.LocalizeFields, info.LocalizeProperties);
                }
            }

            return info;
        }

        /// <summary>
        /// Exclude the specified type. Overrides other rules.
        /// </summary>
        /// <param name="type">The type to exclude.</param>
        public void Exclude(Type type)
        {
            if (type == null)
            {
                throw new NullReferenceException("Localizer cannot exclude null type.");
            }
            if (TypesExcluded.Contains(type))
            {
                throw new NullReferenceException($"Localizer already excludes type {type}.");
            }
            TypesExcluded.Add(type);
        }
        /// <summary>
        /// Exclude the specified object. Overrides other rules.
        /// </summary>
        /// <param name="object">The object to exclude.</param>
        public void Exclude(object @object)
        {
            if (@object == null)
            {
                throw new NullReferenceException("Localizer cannot exclude null object.");
            }
            if (ObjectsExcluded.Contains(@object))
            {
                throw new NullReferenceException($"Localizer already excludes object {@object}.");
            }
            ObjectsExcluded.Add(@object);
        }

        #region Util
        private static List<Type> GetInheritedTypesAndInterfaces(Type type)
        {
            List<Type> types = new List<Type>();

            // Add all inherited types including the one provided
            Type baseType = type;
            while (baseType != null)
            {
                types.Add(baseType);
                baseType = baseType.BaseType;
            }

            // Add interfaces
            types.AddRange(type.GetInterfaces());

            return types;
        }
        #endregion
    }
}
