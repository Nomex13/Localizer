﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Iodynis.Libraries.Localizing.Example
{
    public class LanguageNativeNameValueConverter : IValueConverter
    {
        public LanguageNativeNameValueConverter()
        {
            ;
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return LocalizationManager.Localizer.LocalizeKey("Language.NameFull.Native", (string)value);
        }
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException($"{GetType()} can only be used for one way conversion.");
        }
    }
}
