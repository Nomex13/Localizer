﻿using System.Windows;

namespace Iodynis.Libraries.Localizing.Example
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            ComboBoxLanguage.SelectedItem = LocalizationManager.Localizer.Settings.Language;

            // Need to localize title separately
            LocalizationManager.Hooker.Hook(this, nameof(Title)).Activate();
        }

        private void ComboBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs arguments)
        {
            if (arguments.AddedItems.Count != 1)
            {
                return;
            }

            string language = arguments.AddedItems[0].ToString();
            LocalizationManager.Localizer.Settings.Language = language;
        }

        private void ButtonOk_Click(object sender, RoutedEventArgs e)
        {
            ;
        }
        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
