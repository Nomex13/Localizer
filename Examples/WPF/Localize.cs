﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;

namespace Iodynis.Libraries.Localizing.Example
{
    [MarkupExtensionReturnType(typeof(Binding))]
    public class Localize : MarkupExtension
    {
        [ConstructorArgument("Text")]
        public string Text { get; set; }
        [ConstructorArgument("Values")]
        public string[] Values { get; set; } = new string[0];
        public Localize()
        {
            ;
        }
        public Localize(object text)
        {
	        Text = text.ToString();
        }
        public Localize(string text)
        {
	        Text = text;
        }
        public Localize(string text, params string[] values)
        {
	        Text = text;
            Values = values;
        }
        public Localize(string[] textAndValues)
        {
            Text = textAndValues[0];
            Values = new string[textAndValues.Length - 1];
            textAndValues.CopyTo(Values, 1);
        }
        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            // Hook localizer to the target property
            IProvideValueTarget provideValueTarget = serviceProvider.GetService(typeof(IProvideValueTarget)) as IProvideValueTarget;
            if (provideValueTarget != null)
            {
                DependencyObject targetObject = provideValueTarget.TargetObject as DependencyObject;
                // In design mode show the key
                if (DesignerProperties.GetIsInDesignMode(targetObject))
                {
                    return GetTextInDesignMode(Text);
                }
                // In runtime show the localized value
                if (targetObject != null)
                {
                    DependencyProperty targetProperty = provideValueTarget.TargetProperty as DependencyProperty;
                    LocalizationManager.Hooker.Hook((language) =>
                    {
                        string @string = LocalizationManager.Localizer.LocalizeString(Text, language, Values ?? new string[0], out List<string> keys);
                        return new Tuple<string, List<string>>(@string, keys);
                    },
                    (@string) =>
                    {
                        targetObject.SetValue(targetProperty, @string);
                    }).Activate();
                }
            }
            // This time (that is the first time) return the localized string
            return LocalizationManager.Localizer.LocalizeString(Text, Values ?? new string[0]);
        }
        private string GetTextInDesignMode(string text)
        {
            StringBuilder stringBuilder = new StringBuilder(text.Length);
            bool isKey = false;
            int indexKeyStart = 0;
            for (int index = 0;  index < text.Length; index++)
            {
                char character = text[index];
                if (character == '[')
                {
                    isKey = true;
                    indexKeyStart = index + 1;
                }
                else if (character == ']')
                {
                    isKey = false;
                    string key = text.Substring(indexKeyStart, index - indexKeyStart);
                    key = key.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries).Last();
                    stringBuilder.Append(key);
                }
                else if (!isKey)
                {
                    stringBuilder.Append(character);
                }
            }
            return stringBuilder.ToString();
        }
    }
}
