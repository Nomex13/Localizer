﻿using System.ComponentModel;
using System.Windows;

namespace Iodynis.Libraries.Localizing.Example
{
    public static class LocalizationManager
    {
        public static LocalizerHooker Hooker { get; }
        public static Localizer Localizer { get; }
        static LocalizationManager()
        {
            // Do not run localizer in design mode.
            // The Localize markup extension will handle the text display.
            if (DesignerProperties.GetIsInDesignMode(new DependencyObject()))
            {
                return;
            }

            Localizer = new Localizer("../../Localization.csv");

            Localizer.Settings.FallbackColumn = "fallback";
            Localizer.Settings.FallbackLanguagesKey = "Language.Fallback";
            Localizer.Settings.PriorityKey = "Language.Priority";
            Localizer.Settings.Language = Localizer.LanguageClosest("en");

            // In WPF curvy brackets { and } are used by XAML, so to minimize trouble use square brackets [ and ] instead
            Localizer.Settings.Bra = "[";
            Localizer.Settings.Ket = "]";

            Hooker = new LocalizerHooker(Localizer);
        }
    }
}
