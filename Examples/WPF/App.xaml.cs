﻿using System;
using System.Windows;

namespace Iodynis.Libraries.Localizing.Example
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs arguments)
        {
            base.OnStartup(arguments);

            Console.WriteLine("Nya");
        }
        protected override void OnExit(ExitEventArgs arguments)
        {
            base.OnExit(arguments);
        }
    }
}
