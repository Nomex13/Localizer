﻿using System;

namespace Iodynis.Libraries.Localizing.Example
{
    static class Program
    {
        public static void Main(params string[] arguments)
        {
            Localizer localizer = new Localizer("../../Localization.csv");

            localizer.Settings.FallbackColumn = "fallback";
            localizer.Settings.FallbackLanguage = localizer.LanguageClosest("en");
            localizer.Settings.FallbackLanguagesKey = "Language.Fallback";
            localizer.Settings.PriorityKey = "Language.Priority";

            localizer.Storage.Set("VersionMajor", 1);
            localizer.Storage.Set("VersionMinor", 0);

            var snapshot = localizer.LocalizeKeySnapshot("TwoArguments", 10, 500);
            Console.WriteLine(snapshot["en-GB"]);

            Console.WriteLine(localizer.LocalizeString("{Version}"));

            Console.ReadKey();
        }
    }
}