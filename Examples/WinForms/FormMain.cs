﻿using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Iodynis.Libraries.Localizing.Example
{
    public partial class FormMain : Form
    {
        private struct Language
        {
            public string Code { get; private set; }
            public string Name { get; private set; }
            public Language(string code, string name)
            {
                Code = code;
                Name = name;
            }
        }
        public FormMain()
        {
            InitializeComponent();

            // Setup the language list
            comboBoxLanguage.DataSource = LocalizationManager.Localizer.Languages.Select(_language => new Language(_language, LocalizationManager.Localizer.LocalizeKey("Language.NameFull.Native", _language))).ToList();
            comboBoxLanguage.DisplayMember = "Name";
            comboBoxLanguage.ValueMember = "Code";
            comboBoxLanguage.SelectedValueChanged += ComboBoxLanguage_SelectedValueChanged;

            radioButtonMale.Checked = true;
            textBoxCount.Text = "1";

            LocalizationManager.Walker.WalkAndHook(this, LocalizationManager.Hooker);
            LocalizationManager.Hooker.Localize();
        }

        private void ComboBoxLanguage_SelectedValueChanged(object sender, EventArgs e)
        {
            LocalizationManager.Localizer.Settings.Language = comboBoxLanguage.SelectedValue.ToString();
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void textBoxCount_TextChanged(object sender, EventArgs e)
        {
            LocalizationManager.Localizer.Storage.Set("MyCount", ((TextBox)sender).Text);
        }

        private void radioButtonMale_CheckedChanged(object sender, EventArgs e)
        {
            LocalizationManager.Localizer.Storage.Set("MySex", 1);
        }

        private void radioButtonFemale_CheckedChanged(object sender, EventArgs e)
        {
            LocalizationManager.Localizer.Storage.Set("MySex", 0);
        }
    }
}
