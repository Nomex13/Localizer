﻿using System;
using System.Windows.Forms;

namespace Iodynis.Libraries.Localizing.Example
{
    public static class LocalizationManager
    {
        public static LocalizerWalker Walker { get; }
        public static LocalizerHooker Hooker { get; }
        public static Localizer Localizer { get; }
        static LocalizationManager()
        {
            Localizer = new Localizer("../../Localization.csv");

            Localizer.Settings.FallbackColumn = "fallback";
            Localizer.Settings.FallbackLanguagesKey = "Language.Fallback";
            Localizer.Settings.PriorityKey = "Language.Priority";
            Localizer.Settings.Language = Localizer.LanguageClosest("en");
            Localizer.Settings.ErrorSubstitution = " [ERROR] ";

            Walker = new LocalizerWalker(Localizer);
            // Walk
            Walker.Walk(typeof(Form), typeof(Form), typeof(Control));
            Walker.Walk(typeof(Control), typeof(Control));
            // Localize
            Walker.Localize(typeof(Form), nameof(Form.Text));
            Walker.Localize(typeof(Label), nameof(Label.Text));
            Walker.Localize(typeof(Button), nameof(Button.Text));
            Walker.Localize(typeof(CheckBox), nameof(CheckBox.Text));
            Walker.Localize(typeof(RadioButton), nameof(RadioButton.Text));
            Walker.Initialize();

            Hooker = new LocalizerHooker(Localizer);

            Localizer.Error += LocalizerError;
            //Walker.Error += LocalizerError;
            Hooker.Error += LocalizerError;

            //Localizer.Settings.FunctionAdd(new LocalizerFunctionPlural());
            //Localizer.Settings.FunctionAdd(new LocalizerFunctionEnumeration("Sex"));
        }

        private static void LocalizerError(object sender, Exception exception)
        {
            Console.WriteLine(exception.Message);
        }
    }
}
