# Localizer

A localization library.

Supports arguments, constants, plural forms, custom functions.
Has a hook module that updates properties and fields whenever the language changes.
Has a walker module that walks through the hierarchy according to rules you set and hooks fields and properties.
Has functionality to create snapshots of localized strings with all substituted values used, useful in case arguments and constants change over time and you need a way to roll back.

## Dependencies

Localizer is dependent on the https://gitlab.com/iodynis/Csver and the https://gitlab.com/iodynis/Parser projects.

## Usage

Create the localizer:

```csharp
Localizer localizer = new Localizer("Localization.csv");
```

Set options:

```csharp
Localizer.Settings.FallbackColumn = "fallback";
Localizer.Settings.FallbackLanguagesKey = "Language.Fallback";
Localizer.Settings.PriorityKey = "Language.Priority";
```

Set the language to the specific one:

```csharp
Localizer.Settings.Language = "en-GB";
```

Or set the language safely to the one that is closest to what you specify:

```csharp
Localizer.Settings.Language = Localizer.LanguageClosest("en");
```

Set constants:

```csharp
localizer.Storage.Set("VersionMajor", 1);
localizer.Storage.Set("VersionMinor", 0);
```

Create a snapshot that will store the current argument values and constants used so that you can localize that state any time in the future:

```csharp
var descriptionSnapshot = localizer.LocalizeKeySnapshot("Description", 10, 500);
Console.WriteLine(descriptionSnapshot["en"]);
```

## Examples

3 example projects are provided to explore the functionality of the library.

### Console

Use `LocalizeString` and `LocalizeKey` methods:

```csharp
Console.WriteLine(localizer.LocalizeString("Version: {VersionMajor}.{VersionMinor}"));
Console.WriteLine(localizer.LocalizeKey("VersionMajor"));
```

### WinForms

Example project shows some features:

![WinForms example screenshot](/Examples/WinForms/Screenshot.png)

Setup a walker that will walk the WinForms controls:

```csharp
Walker = new LocalizerWalker(Localizer);
// Walk
Walker.Walk(typeof(Form), typeof(Form), typeof(Control));
Walker.Walk(typeof(Control), typeof(Control));
// Localize
Walker.Localize(typeof(Form), nameof(Form.Text));
Walker.Localize(typeof(Label), nameof(Label.Text));
Walker.Localize(typeof(Button), nameof(Button.Text));
Walker.Localize(typeof(CheckBox), nameof(CheckBox.Text));
Walker.Localize(typeof(RadioButton), nameof(RadioButton.Text));
Walker.Initialize();
```

Call the walker to localize once or bind to relocalize every time ```Localizer.Settings.Language``` changes:

```csharp
Walker.WalkLocalize(this); // Localize once
Walker.WalkAndHook(this).Localize(); // Localize every time the language or stored variables change
```

### WPF

Example project shows some features:

![WPF example screenshot](/Examples/WPF/Screenshot.png)

Set the localizer to use the square brackets to avoid conflicts with XAML which uses the curvy ones:

```csharp
Localizer.Settings.Bra = "[";
Localizer.Settings.Ket = "]";
```

You will need to manually hook the title of the window to the localizer:

```csharp
LocalizationManager.Localizer.Hooker.Hook(this, nameof(Title)).Activate();
```

To localize other GUI elements you will need to create a `MarkupExtension`, sample code available in the example here https://gitlab.com/iodynis/Localizer/-/blob/master/Examples/WPF/LocalizerMarkupExtension.cs .
Then you can reference this extension in XAML like this: `<Label Content="{local:Localize '[Description]'}" />`.

## License

Library is available under the MIT license.

Repository icon is from https://ikonate.com/ pack and is used under the MIT license.